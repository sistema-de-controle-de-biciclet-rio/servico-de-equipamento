require("dotenv").config();
const { json } = require("express");
const BicicletaController = require("../../../src/controllers/bicicleta.controller.js");
const Bicicleta = require("../../../src/entities/bicicleta.entity.js");
const { get } = require("../../../src/routes/bicicleta.routes.js");

describe("BicicletaController", () => {
    let bicicletaRepository;
    let bicicletaController;
    let axios;
    let registroInsercaoBicicletaTrancaRepository;
    let registroRetiradaBicicletaTrancaRepository;
    let trancaRepository;

    beforeEach(async () => {
        const DataSource = {
            tabelas: {
                bicicleta: []
            },
            getRepository: jest.fn((tabela) => {
                return {
                    salvar: jest.fn(dados => {
                        return new Bicicleta(dados);
                    }),
                    buscarPorId: jest.fn((id) => (new Bicicleta({ 
                        marca: "Specialized",
                        modelo: "Rockhopper",
                        ano: "2020",
                        status: "NOVA" ,

                    }))),
                    buscarTodas: jest.fn(() => ([new Bicicleta({ 
                        marca: "Specialized",
                        modelo: "Rockhopper",
                        ano: "2020",
                        status: "NOVA" ,

                    })])),
                    remover: jest.fn((id) => (new Bicicleta({ 
                        marca: "Specialized",
                        modelo: "Rockhopper",
                        ano: "2020",
                        status: "NOVA" ,
 
                    }))),
                    atualizar: jest.fn((id) => (new Bicicleta({ 
                        marca: "Trek",
                        modelo: "Fuel EX",
                        ano: "2020",
                        status: "NOVA" ,
 
                    })))
                };
            }) 
        }

        bicicletaRepository = DataSource.getRepository("bicicleta");
        trancaRepository = DataSource.getRepository("tranca");
        registroInsercaoBicicletaTrancaRepository = DataSource.getRepository("registroInsercaoBicicletaTranca");
        registroRetiradaBicicletaTrancaRepository = DataSource.getRepository("registroRetiradaBicicletaTranca");

        bicicletaRepository.buscarPorId.mockReset();
        registroInsercaoBicicletaTrancaRepository.salvar.mockReset();

        axios = {
            get: jest.fn(),
            post: jest.fn()
        };

        bicicletaController = new BicicletaController(bicicletaRepository, axios, registroInsercaoBicicletaTrancaRepository, trancaRepository, registroRetiradaBicicletaTrancaRepository); 
       
    })
    
    test("Deve cadastrar uma nova bicicleta.", async () => {
        const requestMock = {
            body: {
                marca: "Specialized",
                modelo: "Rockhopper",
                ano: "2020",
                status: "NOVA"
            }
        };

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

     

        
        await bicicletaController.postBicicleta(requestMock, responseMock);

        expect(bicicletaRepository.salvar).toBeCalledWith(requestMock.body);
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.json).toBeCalledWith({ id: 1, ...requestMock.body });
    })

    test("Deve cadastrar uma bicicleta (erro)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                marca: "Specialized",
                modelo: "Rockhopper",
                ano: "2020",
                status: "NOVA"
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        bicicletaRepository.salvar.mockImplementation(() => {throw Error ()});

        await bicicletaController.postBicicleta(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(422);
    });

    test("Deve retornar uma bicicleta", async () => {
        bicicletaRepository.buscarPorId.mockReturnValue(new Bicicleta({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "NOVA"
        }));

        const requestMock = {
            params: {
                idBicicleta: 1
            }
        };

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        
        await bicicletaController.getBicicleta(requestMock, responseMock);

        expect(bicicletaRepository.buscarPorId).toBeCalledWith(requestMock.params.idBicicleta);
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve retornar uma bicicleta", async () => {
        const requestMock = {
            params: {
                idBicicleta: 1
            }
        };

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        bicicletaRepository.buscarPorId.mockReturnValue(null);
        
        await bicicletaController.getBicicleta(requestMock, responseMock);

        expect(bicicletaRepository.buscarPorId).toBeCalledWith(requestMock.params.idBicicleta);
        expect(responseMock.status).toBeCalledWith(404);
    });

    test("Deve retornar todas as bicicletas", async () => {
        const requestMock = {
            body: {
                marca: "Specialized",
                modelo: "Rockhopper",
                ano: "2020",
                status: "NOVA"
            }
        };

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        
        await bicicletaController.getBicicletas(requestMock, responseMock);

        expect(bicicletaRepository.buscarTodas).toBeCalledWith();
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve deletar uma bicicleta", async () => {
        const requestMock = {
            params: {
                idBicicleta: 1
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock),
            end: jest.fn()
        };
    
        jest.spyOn(bicicletaRepository, 'remover').mockReturnValue(true);

        bicicletaRepository.buscarPorId.mockReturnValue(new Bicicleta({ 
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "NOVA"
        }));

        await bicicletaController.deleteBicicleta(requestMock, responseMock);

        expect(bicicletaRepository.remover).toBeCalledWith(requestMock.params.idBicicleta);
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.end).toBeCalled();

    });

    test("Deve deletar uma bicicleta (erro)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock),
            end: jest.fn(content => responseMock),
        };

        const json = {
            codigo: 404,
            mensagem: "Bicicleta não encontrada."
        };

        bicicletaRepository.buscarPorId.mockReturnValue(null);

        bicicletaRepository.remover.mockReturnValue(false);

        await bicicletaController.deleteBicicleta(requestMock, responseMock);

        expect(bicicletaRepository.remover).not.toBeCalledWith(requestMock.params.idBicicleta);
        expect(responseMock.status).toBeCalledWith(404);
        expect(responseMock.json).toBeCalledWith(json);

    });

    test('Deve retornar erro ao deletar a bicicleta, pois ela está em uso', async () => {
        const request = {
            params: {
                idBicicleta: 1
            }
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        bicicletaRepository.buscarPorId.mockReturnValue({
            getStatus: jest.fn().mockReturnValue("EM_USO"),
        });

        await bicicletaController.deleteBicicleta(request, response);
        
        expect(response.status).toHaveBeenCalledWith(404);
        expect(response.json).toHaveBeenCalledWith({
            codigo: 404,
            mensagem: 'Bicicleta não pode ser excluida, pois está em uma tranca ou não está aposentada.',
        });
    });

    test("Deve atualizar uma bicicleta", async () => {
        const requestMock = {
            params: {
                idBicicleta: 1
            },
            body: {
                id: 1,
                marca: "Trek",
                modelo: "Fuel EX",
                ano: "2020",
                status: "NOVA"
            }
        };
        
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
        
        const bicicleta = new Bicicleta({
            marca: "Trek",
            modelo: "Fuel EX",
            ano: "2020",
            status: "NOVA"
        });
        
        bicicletaRepository.atualizar.mockReturnValue(bicicleta);
        
        await bicicletaController.atualizarBicicleta(requestMock, responseMock);
        
        expect(bicicletaRepository.atualizar).toBeCalledWith(requestMock.params.idBicicleta, requestMock.body);
        expect(responseMock.status).toBeCalledWith(200);
    });
    
   test("Deve atualizar uma bicicleta (erro)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                marca: "Trek",
                modelo: "Fuel EX"
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        bicicletaRepository.atualizar.mockImplementation(() => {throw Error ()});
    
        await bicicletaController.atualizarBicicleta(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(422);
    });

    test("Deve atualizar uma bicicleta (erro null)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                marca: "Trek",
                modelo: "Fuel EX"
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        bicicletaRepository.atualizar.mockImplementation(() => null);
    
        await bicicletaController.atualizarBicicleta(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(404);
    });

    test('Deve integrar a bicicleta na rede com sucesso', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            integrarNaRede: jest.fn().mockReturnValue(true),
        });

        const request = {
            body: {
                idBicicleta: 1,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            end: jest.fn(),
            json: jest.fn().mockReturnThis(),
        };

        axios.get.mockReturnValue({success: true, data: {email:"teste"} });
        axios.post.mockResolvedValue({ success: true })
        const conteudo = {data: (new Date ()).toLocaleString(), idTranca: request.body.idTranca, idBicicleta: request.body.idBicicleta, toObject(){}};

        registroInsercaoBicicletaTrancaRepository.salvar.mockResolvedValue(conteudo);
        
        await bicicletaController.integrarBicicletaNaRede(request, response);

        expect(bicicletaRepository.buscarPorId).toHaveBeenCalledWith(1);
        expect(bicicletaRepository.atualizar).toHaveBeenCalledWith(1, { idTranca: 1, status: "DISPONIVEL" });
        expect(trancaRepository.atualizar).toHaveBeenCalledWith(1, { idBicicleta: 1, status: "OCUPADA" });
        expect(axios.post).toBeCalledWith(`${process.env.EXTERNO_URL}/enviarEmail`, {assunto: "Bicicleta integrada na rede", mensagem: JSON.stringify(conteudo.toObject()), email: "teste"});
        expect(registroInsercaoBicicletaTrancaRepository.salvar).toHaveBeenCalledWith({
            data: expect.any(String),
            idTranca: 1,
            idBicicleta: 1,
        });
        expect(response.status).toHaveBeenCalledWith(200);
    });

    test('Deve retornar erro ao integrar a bicicleta na rede', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            integrarNaRede: jest.fn().mockReturnValue(false),
        });

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await bicicletaController.integrarBicicletaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: "A bicicleta ou está em uso, ou está em reparo e o funcionário que tentou integra-la nao foi o mesmo que a retirou da rede.",
        }]);
    });

    test('Deve retornar erro ao integrar a bicicleta na rede, pois o funcionário não existe', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            integrarNaRede: jest.fn().mockReturnValue(true),
            getIdTranca: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("REPARO_SOLICITADO"),
        });

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 0,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
            end: jest.fn().mockReturnThis(),
        };

        axios.get.mockReturnValue({success: false, data: {email:"teste"} });

        await bicicletaController.integrarBicicletaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Funcionário não encontrado.',
        }]);
    });

    test('Deve retornar erro ao integrar a bicicleta da rede, pois ela não existe', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await bicicletaController.integrarBicicletaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Bicicleta não encontrada.',
        }]);
    });

    test('Deve retirar a bicicleta da rede com sucesso', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(true),
            getIdTranca: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("REPARO_SOLICITADO"),
        });

        trancaRepository.buscarPorId.mockReturnValue({
            getId: jest.fn().mockReturnValue(1),
        });

        const request = {
            body: {
                idTranca: -1,
                idBicicleta: 1,
                idFuncionario: 1,
                status: "APOSENTADA"
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            end: jest.fn(),
            json: jest.fn().mockReturnThis(),
        };

        axios.get.mockReturnValue({success: true, data: {email:"teste"} });
        axios.post.mockResolvedValue({ success: true })

        const conteudo = {data: (new Date ()).toLocaleString(), idBicicleta: request.body.idBicicleta, idReparador: request.body.idFuncionario, toObject(){}};

        registroRetiradaBicicletaTrancaRepository.salvar.mockResolvedValue(conteudo);

        await bicicletaController.retirarBicicletaDaRede(request, response);

        expect(bicicletaRepository.buscarPorId).toHaveBeenCalledWith(1);
        expect(bicicletaRepository.atualizar).toHaveBeenCalledWith(1, { idTranca: -1, status: request.body.status});
        expect(trancaRepository.atualizar).toHaveBeenCalledWith(1, { idBicicleta: -1, status: "DISPONIVEL" });
        expect(axios.post).toBeCalledWith(`${process.env.EXTERNO_URL}/enviarEmail`, {assunto: "Bicicleta retirada da rede", mensagem: JSON.stringify(conteudo.toObject()), email: "teste"});
        expect(registroRetiradaBicicletaTrancaRepository.salvar).toHaveBeenCalledWith({
            data: (new Date ()).toLocaleString(),
            idReparador: 1,
            idBicicleta: 1,
        });
        expect(response.status).toHaveBeenCalledWith(200);
    });

    test('Deve retornar erro ao retirar a bicicleta da rede, pois ela não está na rede', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(false),
            getIdTranca: jest.fn().mockReturnValue(-1),
        });

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await bicicletaController.retirarBicicletaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Bicicleta não está na rede.',
        }]);
    });

    test('Deve retornar erro ao retirar a bicicleta da rede, pois ela não existe', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await bicicletaController.retirarBicicletaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Bicicleta não encontrada.',
        }]);
    });

    test('Deve retornar erro ao retirar a bicicleta da rede, pois o funcionário não existe', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(true),
            getIdTranca: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("REPARO_SOLICITADO"),
        });

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 0,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
            end: jest.fn().mockReturnThis(),
        };

        axios.get.mockReturnValue({success: false, data: {email:"teste"} });

        await bicicletaController.retirarBicicletaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Funcionário não encontrado.',
        }]);
    });

    test('Deve retornar erro ao retirar a bicicleta da rede, pois a tranca não existe', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(true),
            getIdTranca: jest.fn().mockReturnValue(1),
        });

        trancaRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await bicicletaController.retirarBicicletaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Tranca não encontrada.',
        }]);
    });

    test('Deve retornar erro ao retirar a bicicleta da rede, pois a bicicleta está em reparo', async () => {
        bicicletaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(true),
            getIdTranca: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("EM_USO"),
        });

        trancaRepository.buscarPorId.mockReturnValue({
            getId: jest.fn().mockReturnValue(1),
        });

        const request = {
            body: {
                idBicicleta: 0,
                idFuncionario: 1,
                idTranca: 1,
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await bicicletaController.retirarBicicletaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Não é possível retirar bicicleta da rede, pois é necessário solicitar o reparo antes.',
        }]);
    });


    test("Deve atualizar o status de uma bicicleta", async () => {
        const request = {
            params: {
                idBicicleta: 1,
                acao: "DISPONIVEL"
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const bicicleta = new Bicicleta({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "OCUPADA"
        });
    
        bicicletaRepository.atualizar.mockReturnValue(bicicleta);
    
        await bicicletaController.atualizarStatusBicicleta(request, response);
    
        expect(bicicletaRepository.atualizar).toBeCalledWith(request.params.idBicicleta, { status: request.params.acao });
        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(bicicleta.toObject());
    });

    test('Deve retornar erro ao atualizar o status da bicicleta (bicicleta null)', async () => {
        const request = {
            params: {
                idBicicleta: 0
            },
            body: {
                status: 'OCUPADA'
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        bicicletaRepository.atualizar.mockImplementation(() => null);
    
        await bicicletaController.atualizarStatusBicicleta(request, response);
    
        expect(response.status).toBeCalledWith(404);
        expect(response.json).toHaveBeenCalledWith({
            codigo: 404,
            mensagem: 'Bicicleta não encontrada.',
        });
    });

  
})