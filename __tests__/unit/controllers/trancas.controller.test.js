require("dotenv").config();
const e = require("express");
const TrancaController = require("../../../src/controllers/tranca.controller.js");
const Tranca = require("../../../src/entities/tranca.entity.js");

describe("TrancaController", () => {
    let trancaRepository;
    let trancaController;
    let Repository;
    let axios;
    let registroInsercaoTrancaTotemRepository;
    let totemRepository;
    let registroRetiradaTrancaTotemRepository;
    let bicicletaRepository

    beforeEach(async () => {
        const DataSource = {
            tabelas: {
                bicicleta: []
            },
            getRepository: jest.fn((tabela) => {
                return {
                    salvar: jest.fn(dados => {
                        return new Tranca(dados);
                    }),
                    buscarTodas: jest.fn(() => ([new Tranca({ 
                        localizacao: "rio de janeiro",
                        anoDeFabricacao: "2022",
                        modelo: "tranca",
                        status: "NOVA"
                    })])),
                    remover: jest.fn((id) => (new Tranca({ 
                        localizacao: "rio de janeiro",
                        anoDeFabricacao: "2022",
                        modelo: "tranca",
                        status: "NOVA"
                    }))),
                    atualizar: jest.fn((id) => (new Tranca({ 
                        localizacao: "rio de janeiro",
                        anoDeFabricacao: "2022",
                        modelo: "tranca",
                        status: "NOVA"
                    }))),
                    buscarPorId: jest.fn((id) => (new Tranca({
                        localizacao: "rio de janeiro",
                        anoDeFabricacao: "2022",
                        modelo: "tranca",
                        status: "NOVA",
                    })))
                };
            }) 
        }

        trancaRepository = DataSource.getRepository("tranca");
        totemRepository = DataSource.getRepository("totem");
        registroInsercaoTrancaTotemRepository = DataSource.getRepository("registroInsercaoTrancaTotem");
        registroRetiradaTrancaTotemRepository = DataSource.getRepository("registroRetiradaTrancaTotem");
        bicicletaRepository = DataSource.getRepository("bicicleta");

        axios = {
            get: jest.fn(),
            post: jest.fn()
        };

        trancaController = new TrancaController(trancaRepository, axios, registroInsercaoTrancaTotemRepository, totemRepository, registroRetiradaTrancaTotemRepository, bicicletaRepository); 
       
    })
    
    test("Deve cadastrar uma nova tranca.", async () => {
        const requestMock = {
            body: {
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "NOVA",
            }
        }; 

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        await trancaController.postTranca(requestMock, responseMock);

        expect(trancaRepository.salvar).toBeCalledWith(requestMock.body);
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.json).toBeCalledWith({ id: 1, ...requestMock.body });
    });

    test("Deve cadastrar uma nova tranca(erro)", async () => {
        const requestMock = {
            body: {
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "NOVA",
            }
        }; 
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        trancaRepository.salvar.mockImplementation(() => {throw Error ()});
    
        await trancaController.postTranca(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(422);
    });

    test("Deve retornar todas as trancas.", async () => {
        const requestMock = {
            body: {
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "NOVA",
            }
        };

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        await trancaController.getTrancas(requestMock, responseMock);

        expect(trancaRepository.buscarTodas).toBeCalledWith();
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve retornar uma tranca", async () => {
        const requestMock = {
            params: {
                idTranca: 3
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        await trancaController.getTranca(requestMock, responseMock);

        expect(trancaRepository.buscarPorId).toBeCalledWith(requestMock.params.idTranca);
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve retornar uma tranca (erro)", async () => {
        const requestMock = {
            params: {
                idTranca: 0
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        trancaRepository.buscarPorId.mockReturnValue(null);

        await trancaController.getTranca(requestMock, responseMock);

        expect(trancaRepository.buscarPorId).toBeCalledWith(requestMock.params.idTranca);
        expect(responseMock.status).toBeCalledWith(404);
    });

    test("Deve deletar uma tranca", async () => {
        const requestMock = {
            params: {
                idTranca: 1
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock),
            end: jest.fn()
        };

        await trancaController.deleteTranca(requestMock, responseMock);

        expect(trancaRepository.remover).toBeCalledWith(requestMock.params.idTranca);
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.end).toBeCalled();

    });

    test("Deve deletar uma tranca (erro)", async () => {
        const requestMock = {
            params: {
                idTranca: 0
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock),
        };

        const json = {
            codigo: 404,
            mensagem: "Tranca não encontrada."
        };

        trancaRepository.remover.mockReturnValue(false);

        await trancaController.deleteTranca(requestMock, responseMock);

        expect(trancaRepository.remover).toBeCalledWith(requestMock.params.idTranca);
        expect(responseMock.status).toBeCalledWith(404);
        expect(responseMock.json).toBeCalledWith(json);

    });

    test("Deve atualizar uma tranca", async () => {
        const requestMock = {
            params: {
                idTranca: 1
            },
            body: {
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "OCUPADA"
            }
        };
        
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
        
        const tranca = new Tranca({
            id: 1,
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "OCUPADA"
        });
        
        trancaRepository.atualizar.mockReturnValue(tranca);
        
        await trancaController.atualizarTranca(requestMock, responseMock);
        
        expect(trancaRepository.atualizar).toBeCalledWith(requestMock.params.idTranca, requestMock.body);
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve atualizar uma tranca(erro)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                id: 1,
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "NOVA"
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        trancaRepository.atualizar.mockImplementation(() => {throw Error ()});
    
        await trancaController.atualizarTranca(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(422);
    });

    test("Deve atualizar uma tranca(erro null)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                id: 1,
                localizacao: "rio de janeiro",
                anoDeFabricacao: "2022",
                modelo: "tranca",
                status: "NOVA"
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        trancaRepository.atualizar.mockImplementation(() => null);
    
        await trancaController.atualizarTranca(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(404);
    });

    test('Deve integrar a tranca na rede com sucesso', async () => {
        trancaRepository.buscarPorId.mockReturnValue({
            integrarNaRede: jest.fn().mockReturnValue(true),
        });

        const request = {
            body: {
                idTranca: 1,
                idTotem: 1,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            end: jest.fn(),
            json: jest.fn().mockReturnThis(),
        };

        axios.get.mockReturnValue({success: true, data: {email:"teste@gmail.com"} });
        axios.post.mockResolvedValue({ success: true })

        const conteudo = {data: (new Date ()).toLocaleString(), idTranca: request.body.idTranca, idReparador: request.body.idFuncionario, toObject(){}};

        registroInsercaoTrancaTotemRepository.salvar.mockResolvedValue(conteudo);

        await trancaController.integrarTrancaNaRede(request, response);

        expect(trancaRepository.buscarPorId).toHaveBeenCalledWith(1);
        expect(trancaRepository.atualizar).toHaveBeenCalledWith(1, { idTotem: 1, status: 'DISPONIVEL' });
        expect(axios.post).toBeCalledWith(`${process.env.EXTERNO_URL}/enviarEmail`, {assunto: "Tranca integrada na rede", mensagem: JSON.stringify(conteudo.toObject()), email: "teste@gmail.com"});
        expect(registroInsercaoTrancaTotemRepository.salvar).toHaveBeenCalledWith({
            data: (new Date ()).toLocaleString(),
            idReparador: 1,
            idTranca: 1,
        });
        expect(response.status).toHaveBeenCalledWith(200);
    });

    test('Deve retornar erro ao integrar a tranca na rede (tranca null)', async () => {
        trancaRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idTranca: 0,
                idTotem: 1,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await trancaController.integrarTrancaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Tranca não encontrada.',
        }]);
    });

    test('Deve retornar erro ao integrar a tranca na rede (totem null)', async () => {
        totemRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idTranca: 1,
                idTotem: 0,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await trancaController.integrarTrancaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Totem não encontrado.',
        }]);
    });

    test('Deve retornar erro ao integrar a tranca na rede, pois o funcionário não existe', async () => {
        trancaRepository.buscarPorId.mockReturnValue({
            integrarNaRede: jest.fn().mockReturnValue(true),
            getIdBicicleta: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("DISPONIVEL"),
        });

        const request = {
            body: {
                idTranca: 1,
                idTotem: 0,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
            end: jest.fn(),
        };

        axios.get.mockReturnValue({success: false, data: {email:"teste"} });

        await trancaController.integrarTrancaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Funcionário não encontrado.',
        }]);
    });

    test('Deve retornar erro ao integrar a tranca na rede, pois o funcionário não está autorizado', async () => {
        trancaRepository.buscarPorId.mockReturnValue({
            integrarNaRede: jest.fn().mockReturnValue(false),
            getIdBicicleta: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("DISPONIVEL"),
        });

        const request = {
            body: {
                idTranca: 1,
                idTotem: 0,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await trancaController.integrarTrancaNaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Funcionário não autorizado.',
        }]);
    });

    test("Deve atualizar o status de uma tranca", async () => {
        const requestMock = {
            params: {
                idTranca: 1,
                acao: "DISPONIVEL"
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        const tranca = new Tranca({
            localizacao: "rio de janeiro",
            anoDeFabricacao: "2022",
            modelo: "tranca",
            status: "OCUPADA"
        });
    
        trancaRepository.atualizar.mockReturnValue(tranca);
    
        await trancaController.atualizarStatusTranca(requestMock, responseMock);
    
        expect(trancaRepository.atualizar).toBeCalledWith(requestMock.params.idTranca, { status: requestMock.params.acao });
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.json).toBeCalledWith(tranca.toObject());
    });

    test('Deve retornar erro ao atualizar o status da tranca (tranca null)', async () => {
        const request = {
            params: {
                idTranca: 0
            },
            body: {
                status: 'ocupada'
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        trancaRepository.atualizar.mockImplementation(() => null);
    
        await trancaController.atualizarStatusTranca(request, response);
    
        expect(response.status).toBeCalledWith(404);
        expect(response.json).toHaveBeenCalledWith({
            codigo: 404,
            mensagem: 'Tranca não encontrada.',
        });
    });

    test('Deve retirar a tranca da rede com sucesso', async () => {
        trancaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(true),
        });

        const request = {
            body: {
                idTranca: 1,
                idTotem: 1,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            end: jest.fn(),
            json: jest.fn().mockReturnThis(),
        };

        axios.get.mockReturnValue({success: true, data: {email:"teste"} });
        axios.post.mockResolvedValue({ success: true })

        const conteudo = {data: (new Date ()).toLocaleString(), idTranca: request.body.idTranca, idReparador: request.body.idFuncionario, toObject(){}};

        registroRetiradaTrancaTotemRepository.salvar.mockResolvedValue(conteudo);

        await trancaController.retirarTrancaDaRede(request, response);

        expect(trancaRepository.buscarPorId).toHaveBeenCalledWith(1);
        expect(trancaRepository.atualizar).toHaveBeenCalledWith(1, { idTotem: -1, status: 'APOSENTADA', idFuncionario: 1 });
        expect(axios.post).toBeCalledWith(`${process.env.EXTERNO_URL}/enviarEmail`, {assunto: "Tranca retirada da rede", mensagem: JSON.stringify(conteudo.toObject()), email: "teste"});
        expect(registroRetiradaTrancaTotemRepository.salvar).toHaveBeenCalledWith({
            data: (new Date ()).toLocaleString(),
            idReparador: 1,
            idTranca: 1,
        });
        expect(response.status).toHaveBeenCalledWith(200);
    });

    test('Deve retornar erro ao retirar a tranca da rede (tranca null)', async () => {
        trancaRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idTranca: 0,
                idTotem: 1,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await trancaController.retirarTrancaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Tranca não encontrada.',
        }]);
    });

    test('Deve retornar erro ao retirar a tranca da rede (totem null)', async () => {
        totemRepository.buscarPorId.mockReturnValue(null);

        const request = {
            body: {
                idTranca: 0,
                idTotem: 1,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await trancaController.retirarTrancaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Totem não encontrado.',
        }]);
    });

    test('Deve retornar erro ao retirar a tranca da rede, pois o funcionário não existe', async () => {
        trancaRepository.buscarPorId.mockReturnValue({
            retirarDaRede: jest.fn().mockReturnValue(true),
            getIdBicicleta: jest.fn().mockReturnValue(1),
            getStatus: jest.fn().mockReturnValue("DISPONIVEL"),
        });

        const request = {
            body: {
                idTranca: 1,
                idTotem: 0,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
            end: jest.fn(),
        };

        axios.get.mockReturnValue({success: false, data: {email:"teste"} });

        await trancaController.retirarTrancaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: 'Funcionário não encontrado.',
        }]);
    });

    test('Deve retornar erro ao retirar a tranca da rede (falha)', async () => {
        axios.get.mockReturnValue({success: false});

        const request = {
            body: {
                idTranca: 1,
                idTotem: 1,
                idFuncionario: 1
            },
        };
        const response = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        };

        await trancaController.retirarTrancaDaRede(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith([{
            codigo: 422,
            mensagem: "Para retirar a tranca da rede, é necessário que ela esteja com o status de reparo solicitado, esteja associada a um totem e nao esteja associada a nenhuma bibicleta.",
        }]);
    });

    test("Deve destrancar uma tranca", async () => {
        const request = {
            params: {
                idTranca: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const tranca = new Tranca({
            localizacao: "rio de janeiro",
            anoDeFabricacao: "2022",
            modelo: "tranca",
            status: "DISPONIVEL",
            idReparador: 1
        });
    
        trancaRepository.atualizar.mockReturnValue(tranca);
        registroRetiradaTrancaTotemRepository.salvar.mockReturnValue({});
    
        await trancaController.destrancar(request, response);
    
        expect(trancaRepository.atualizar).toBeCalledWith(request.params.idTranca, { status: "DISPONIVEL" });
        expect(registroRetiradaTrancaTotemRepository.salvar).toBeCalledWith({ data: (new Date ()).toLocaleString(), idReparador: tranca.idReparador, idTranca: request.params.idTranca });
        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(tranca.toObject());
    });

    test("Deve retornar erro ao destrancar uma tranca (tranca null)", async () => {
        trancaRepository.atualizar.mockReturnValue(null);

        const request = {
            params: {
                idTranca: 0
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        await trancaController.destrancar(request, response);
    
        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({
            codigo: 404,
            mensagem: 'Tranca não encontrada.',
        });
    });

    test("Deve trancar uma tranca", async () => {
        const request = {
            params: {
                idTranca: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const tranca = new Tranca({
            localizacao: "rio de janeiro",
            anoDeFabricacao: "2022",
            modelo: "tranca",
            status: "OCUPADA",
            idReparador: 1
        });
    
        trancaRepository.atualizar.mockReturnValue(tranca);
        registroInsercaoTrancaTotemRepository.salvar.mockReturnValue({});
    
        await trancaController.trancar(request, response);
    
        expect(trancaRepository.atualizar).toBeCalledWith(request.params.idTranca, { status: "OCUPADA" });
        expect(registroInsercaoTrancaTotemRepository.salvar).toBeCalledWith({ data: (new Date ()).toLocaleString(), idReparador: tranca.idReparador, idTranca: request.params.idTranca });
        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(tranca.toObject());
    });

    test("Deve retornar erro ao trancar uma tranca (tranca null)", async () => {
        trancaRepository.atualizar.mockReturnValue(null);

        const request = {
            params: {
                idTranca: 0
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        await trancaController.trancar(request, response);
    
        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({
            codigo: 404,
            mensagem: 'Tranca não encontrada.',
        });
    });

    test("Deve retornar as bicicletas de uma tranca", async () => {
        bicicletaRepository.buscarTodas.mockReturnValue([
            {
                toObject: jest.fn()
            }
        ]);

        const request = {
            params: {
                idTranca: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const tranca = new Tranca({
            localizacao: "rio de janeiro",
            anoDeFabricacao: "2022",
            modelo: "tranca",
            status: "OCUPADA",
            idReparador: 1
        });
    
        trancaRepository.buscarPorId.mockReturnValue(tranca);
    
        await trancaController.getBicicletaTranca(request, response);
    
        expect(trancaRepository.buscarPorId).toBeCalledWith(request.params.idTranca);
        expect(bicicletaRepository.buscarTodas).toBeCalledWith({ idTranca: Number.parseInt(request.params.idTranca) });
        expect(response.status).toBeCalledWith(200);
    });

    test("Deve retornar erro ao listar as bicicletas de uma tranca se ela não existe", async () => {
        bicicletaRepository.buscarTodas.mockReturnValue([
            {
                toObject: jest.fn()
            }
        ]);

        const request = {
            params: {
                idTranca: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        trancaRepository.buscarPorId.mockReturnValue(null);
    
        await trancaController.getBicicletaTranca(request, response);
    
        expect(trancaRepository.buscarPorId).toBeCalledWith(request.params.idTranca);
        expect(bicicletaRepository.buscarTodas).not.toBeCalledWith({ idTranca: Number.parseInt(request.params.idTranca) });
        expect(response.json).toBeCalledWith({
            codigo: 422,
            mensagem: "Id da tranca inválido.",
        });
    });

    test("Deve retornar erro ao listar as bicicletas de uma tranca se ela não tem bicicletas", async () => {
        bicicletaRepository.buscarTodas.mockReturnValue([]);

        const request = {
            params: {
                idTranca: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const tranca = new Tranca({
            localizacao: "rio de janeiro",
            anoDeFabricacao: "2022",
            modelo: "tranca",
            status: "OCUPADA",
            idReparador: 1
        });
    
        trancaRepository.buscarPorId.mockReturnValue(tranca);
    
        await trancaController.getBicicletaTranca(request, response);
    
        expect(trancaRepository.buscarPorId).toBeCalledWith(request.params.idTranca);
        expect(bicicletaRepository.buscarTodas).toBeCalledWith({ idTranca: Number.parseInt(request.params.idTranca) });
        expect(response.json).toBeCalledWith({
            codigo: 404,
            mensagem: 'Nenhuma bicicleta encontrada.',
        });
    });

})