require("dotenv").config();
const TotemController = require("../../../src/controllers/totem.controller.js");
const Totem = require("../../../src/entities/totem.entity.js");

describe("TotemController", () => {
    let totemRepository;
    let totemController;
    let trancaRepository;
    let bicicletaRepository;

    beforeEach(async () => {

        Repository = {
            salvar: jest.fn(dados => {
                return new Totem(dados);
            }),
            buscarTodas: jest.fn(() => ([new Totem({ 
                localizacao: "Rio de Janeiro",
                descricao: "totem x"

            })])),
            remover: jest.fn((id) => (new Totem({ 
                localizacao: "Rio de Janeiro",
                descricao: "totem x"
            }))),
            atualizar: jest.fn((id) => (new Totem({ 
                localizacao: "São Paulo",
                descricao: "totem y"
            }))),
            buscarPorId: jest.fn((id) => (new Totem({ 
                localizacao: "São Paulo",
                descricao: "totem y",
                id : 3
            })))
        }

        const DataSource = {
            tabelas: {
                totem: []
            },
            getRepository: jest.fn((tabela) => {
                return Repository;
            })
        }

        totemRepository = DataSource.getRepository("totem");
        trancaRepository = DataSource.getRepository("tranca");
        bicicletaRepository = DataSource.getRepository("bicicleta");
        totemController = new TotemController(totemRepository, trancaRepository, bicicletaRepository); 
    })
    
    test("Deve cadastrar um novo totem", async () => {
        const requestMock = {
            body: {
                localizacao: "Rio de Janeiro",
                descricao: "totem x"
            }
        }; 

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        
        await totemController.postTotem(requestMock, responseMock);

        expect(totemRepository.salvar).toBeCalledWith(requestMock.body);
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.json).toBeCalledWith({ id: 1, ...requestMock.body });
    });

    test("Deve cadastrar um novo totem(erro)", async () => {
        const requestMock = {
            body: {
                localizacao: "Rio de Janeiro",
                descricao: "totem x",
                numero: 1
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        totemRepository.salvar.mockImplementation(() => {throw Error ()});
    
        await totemController.postTotem(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(422);
    });

    test("Deve retornar todos os totens", async () => {
        const requestMock = {
            body: {
                localizacao: "Rio de Janeiro",
                descricao: "totem x",
                numero: 1
            }
        };

        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        
        await totemController.getTotens(requestMock, responseMock);

        expect(totemRepository.buscarTodas).toBeCalled();
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve retornar um totem", async () => {
        const requestMock = {
            params: {
                idTotem: 3
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        await totemController.getTotem(requestMock, responseMock);

        expect(totemRepository.buscarPorId).toBeCalledWith(requestMock.params.idTotem);
        expect(responseMock.status).toBeCalledWith(200);
    });

    test("Deve retornar um totem (erro)", async () => {
        const requestMock = {
            params: {
                idTotem: 3
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };

        totemRepository.buscarPorId.mockReturnValue(null);

        await totemController.getTotem(requestMock, responseMock);

        expect(totemRepository.buscarPorId).toBeCalledWith(requestMock.params.idTotem);
        expect(responseMock.status).toBeCalledWith(404);
    });

    test("Deve deletar um totem", async () => {
        trancaRepository.buscarTodas.mockReturnValue([]);

        const requestMock = {
            params: {
                idTotem: 1
            }
        };
    
        const responseMock = {
            status: jest.fn(() => responseMock),
            json: jest.fn(() => responseMock),
            end: jest.fn()
        };
    
        totemRepository.remover.mockReturnValue(true);
    
        await totemController.deleteTotem(requestMock, responseMock);
    
        expect(totemRepository.remover).toBeCalledWith(requestMock.params.idTotem);
        expect(responseMock.status).toBeCalledWith(200);
        expect(responseMock.end).toBeCalled();
    });
    
    test("Deve deletar um totem (erro)", async () => {
        trancaRepository.buscarTodas.mockReturnValue([]);

        const requestMock = {
            params: {
                idTotem: 0
            }
        };
    
        const responseMock = {
            status: jest.fn(() => responseMock),
            json: jest.fn(() => responseMock)
        };
    
        const json = {
            codigo: 404,
            mensagem: "Totem não encontrado."
        };
    
        totemRepository.remover.mockReturnValue(false);
    
        await totemController.deleteTotem(requestMock, responseMock);
    
        expect(totemRepository.remover).toBeCalledWith(0);
        expect(responseMock.status).toBeCalledWith(404);
        expect(responseMock.json).toBeCalledWith(json);
    });

    test("Deve atualizar um totem", async () => {
        const requestMock = {
            params: {
                idTotem: 1
            },
            body: {
                localizacao: "Rio de Janeiro",
                descricao: "totem x",
                numero: 1
            }
        };
        
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
        
        const totem = new Totem({
            localizacao: "Rio de Janeiro",
            descricao: "totem x",
            numero: 1
        });
        
        totemRepository.atualizar.mockReturnValue(totem);
        
        await totemController.atualizarTotem(requestMock, responseMock);
        
        expect(totemRepository.atualizar).toBeCalledWith(requestMock.params.idTotem, requestMock.body);
        expect(responseMock.status).toBeCalledWith(200);
        });

    test("Deve atualizar um totem(erro)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                localizacao: "Rio de Janeiro",
                descricao: "totem x",
                numero: 1
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        totemRepository.atualizar.mockImplementation(() => {throw Error ()});
    
        await totemController.atualizarTotem(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(422);
    });

    test("Deve atualizar um totem(erro null)", async () => {
        const requestMock = {
            params: {
                idBicicleta: 0
            },
            body: {
                localizacao: "Rio de Janeiro",
                descricao: "totem x",
                numero: 1
            }
        };
    
        const responseMock = {
            status: jest.fn(content => responseMock),
            json: jest.fn(content => responseMock)
        };
    
        totemRepository.atualizar.mockImplementation(() => null);
    
        await totemController.atualizarTotem(requestMock, responseMock);
    
        expect(responseMock.status).toBeCalledWith(404);
    });    

    test("Deve retornar as trancas de um totem", async () => {
        trancaRepository.buscarTodas.mockReturnValue([
            {
                toObject: jest.fn()
            }
        ]);

        const request = {
            params: {
                idTotem: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const totem = new Totem({
            localizacao: "Rio de Janeiro",
            descricao: "totem x",
            id: 1
        });
    
        totemRepository.buscarPorId.mockReturnValue(totem);
    
        await totemController.getTrancasTotem(request, response);
    
        expect(totemRepository.buscarPorId).toBeCalledWith(request.params.idTotem);
        expect(trancaRepository.buscarTodas).toBeCalledWith({ idTotem: Number.parseInt(request.params.idTotem) });
        expect(response.status).toBeCalledWith(200);
    });

    test("Deve retornar erro ao listar as trancas de um totem se ele não existe", async () => {
        trancaRepository.buscarTodas.mockReturnValue([
            {
                toObject: jest.fn()
            }
        ]);

        const request = {
            params: {
                idTotem: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        totemRepository.buscarPorId.mockReturnValue(null);
    
        await totemController.getTrancasTotem(request, response);
    
        expect(totemRepository.buscarPorId).toBeCalledWith(request.params.idTotem);
        expect(trancaRepository.buscarTodas).not.toBeCalledWith({ idTotem: Number.parseInt(request.params.idTotem) });
        expect(response.status).toBeCalledWith(404);
    });

    test("Deve retornar erro ao listar as trancas de um totem se ele não tem trancas", async () => {
        trancaRepository.buscarTodas.mockReturnValue([]);

        const request = {
            params: {
                idTotem: 1
            }
        };

        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };

        const totem = new Totem({
            localizacao: "Rio de Janeiro",
            descricao: "totem x",
            id: 1
        });

        totemRepository.buscarPorId.mockReturnValue(totem);

        await totemController.getTrancasTotem(request, response);

        expect(totemRepository.buscarPorId).toBeCalledWith(request.params.idTotem);
        expect(trancaRepository.buscarTodas).toBeCalledWith({ idTotem: Number.parseInt(request.params.idTotem) });
        expect(response.json).toBeCalledWith({
            codigo: 404,
            mensagem: 'Nenhuma tranca encontrada.',
        });
    });

    test("Deve retornar as bicicletas de um totem", async () => {
        bicicletaRepository.buscarTodas.mockReturnValue([
            {
                toObject: jest.fn()
            }
        ]);

        const request = {
            params: {
                idTotem: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const totem = new Totem({
            localizacao: "Rio de Janeiro",
            descricao: "totem x",
            id: 1
        });
    
        totemRepository.buscarPorId.mockReturnValue(totem);
    
        await totemController.getBicicletasTotem(request, response);
    
        expect(totemRepository.buscarPorId).toBeCalledWith(request.params.idTotem);
        expect(bicicletaRepository.buscarTodas).toBeCalledWith({ idTotem: Number.parseInt(request.params.idTotem) });
        expect(response.status).toBeCalledWith(200);
    });

    test("Deve retornar erro ao listar as bicicletas de um totem pois ele não tem bicicletas", async () => {
        bicicletaRepository.buscarTodas.mockReturnValue([]);
    
        const request = {
            params: {
                idTotem: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        const totem = new Totem({
            localizacao: "Rio de Janeiro",
            descricao: "totem x",
            id: 1
        });
    
        totemRepository.buscarPorId.mockReturnValue(totem);
    
        await totemController.getBicicletasTotem(request, response);
    
        expect(totemRepository.buscarPorId).toBeCalledWith(request.params.idTotem);
        expect(bicicletaRepository.buscarTodas).toBeCalledWith({ idTotem: Number.parseInt(request.params.idTotem) });
        expect(response.json).toBeCalledWith({
            codigo: 404,
            mensagem: 'Nenhuma bicicleta encontrada.',
        });
    });

    test("Deve retornar erro ao listar as bicicletas de um totem pois ele não existe", async () => {
        bicicletaRepository.buscarTodas.mockReturnValue([
            {
                toObject: jest.fn()
            }
        ]);

        const request = {
            params: {
                idTotem: 1
            }
        };
    
        const response = {
            status: jest.fn(content => response),
            json: jest.fn(content => response)
        };
    
        totemRepository.buscarPorId.mockReturnValue(null);
    
        await totemController.getBicicletasTotem(request, response);
    
        expect(totemRepository.buscarPorId).toBeCalledWith(request.params.idTotem);
        expect(bicicletaRepository.buscarTodas).not.toBeCalledWith({ idTotem: Number.parseInt(request.params.idTotem) });
        expect(response.status).toBeCalledWith(404);
    });

})