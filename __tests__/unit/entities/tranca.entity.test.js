const { idBicicleta } = require('../../../src/entities/bicicleta.entity');
const TrancaEntity = require('../../../src/entities/tranca.entity');

describe('TrancaEntity', () => {
    
        it('Deve criar uma entidade de tranca', () => {
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "NOVA" ,
                localizacao: "RJ",
                numero: 1,
                idTotem: 1,
                idBicicleta: 1,
                idFuncionario: 1,
                status: "NOVA",
            });
        
            expect(tranca).toBeInstanceOf(TrancaEntity);
            expect(tranca.getModelo()).toBe('Kryptolok');
            expect(tranca.getAnoDeFabricacao()).toBe('2020');
            expect(tranca.getStatus()).toBe('NOVA');
            expect(tranca.getLocalizacao()).toBe('RJ');
            expect(tranca.getIdTotem()).toBe(1);
            expect(tranca.getIdBicicleta()).toBe(1);
            expect(tranca.getIdFuncionario()).toBe(1);
            expect(tranca.getNumero()).toBe(1); 
            expect(tranca.getStatus()).toBe('NOVA');
        });
    
        it("Deve integrar a tranca na rede", () =>{ 
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "NOVA" ,
                localizacao: "RJ",
                idFuncionario: 1
            });
    
            const res = tranca.integrarNaRede(1);
    
            expect(res).toBe(true);
        });
    
        it("Deve retornar erro ao integrar a tranca na rede, pois o funcionário que está devolvendo a rede não é o mesmo que retirou", () =>{ 
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_REPARO" ,
                localizacao: "RJ",
                idFuncionario: 1
            });
    
            const res = tranca.integrarNaRede(2);
    
            expect(res).toBe(false);
        });
    
        it("Deve retornar erro se a tranca está em uso ao tentar integrar ela na rede", () =>{ 
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_USO" ,
                localizacao: "RJ",
                idFuncionario: 1
            });
    
            const res = tranca.integrarNaRede(1);
    
            expect(res).toBe(false);
        });

        test('Deve retirar a tranca da rede', () => {
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "REPARO_SOLICITADO",
                localizacao: "RJ",
                idFuncionario: 1,
                idBicicleta: -1,
                idTotem: 1,
                numero: 1
            });
    
            const res = tranca.retirarDaRede();
    
            expect(res).toBe(true);
        });

        test('Deve retornar erro ao tentar retirar a tranca da rede, pois ela está em uso', () => {
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_USO",
                localizacao: "RJ",
                idFuncionario: 1,
                idBicicleta: 1,
                idTotem: 1,
                numero: 1
            });
    
            const res = tranca.retirarDaRede(tranca.getId);
    
            expect(res).toBe(false);
        });

        test('Deve retornar os dados da tranca', () => {
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_USO",
                localizacao: "RJ",
                idFuncionario: 1,
                idBicicleta: 1,
                idTotem: 1,
                numero: 1
            });
    
            const res = tranca.getData();
    
            expect(res).toEqual({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_USO",
                localizacao: "RJ",
                idFuncionario: 1,
                idBicicleta: 1,
                idTotem: 1,
                numero: 1,
                id: expect.any(Number)
            });
        });

        test('Deve retornar os dados da tranca', () => {
            const tranca = new TrancaEntity({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_USO",
                localizacao: "RJ",
                idFuncionario: 1,
                idBicicleta: 1,
                idTotem: 1,
                numero: 1
            });
    
            const res = tranca.toObject();
    
            expect(res).toEqual({
                modelo: "Kryptolok",
                anoDeFabricacao: "2020",
                status: "EM_USO",
                localizacao: "RJ",
                numero: 1,
                id: expect.any(Number)
            });
        });
});