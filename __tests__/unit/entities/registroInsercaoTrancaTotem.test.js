const RegistroInsercaoTrancaTotem = require('../../../src/entities/registroInsercaoTrancaTotem.entity');

describe('RegistroInsercaoTrancaTotem', () => {
        
        it('Deve criar um registro de inserção de tranca em totem', () => {
            const registro = new RegistroInsercaoTrancaTotem({
                data: new Date(),
                idTranca: 1,
                idReparador: 1
            });
        
            expect(registro).toBeInstanceOf(RegistroInsercaoTrancaTotem);
            expect(registro.getData()).toEqual({
                id: registro.getId(),
                data: registro._data,   
                idTranca: registro._idTranca,
                idReparador: registro._idReparador
            });
        });

        test('Deve retornar um objeto com os dados do registro', () => {
            const registro = new RegistroInsercaoTrancaTotem({
                data: new Date(),
                idTranca: 1,
                idReparador: 1
            });
        
            expect(registro.toObject()).toEqual({
                data: registro._data,   
                idTranca: registro._idTranca,
                idReparador: registro._idReparador
            });
        });
        
    });