const registroRetiradaTrancaTotem = require('../../../src/entities/registroRetiradaTrancaTotem.entity');

describe('RegistroRetiradaTrancaTotem', () => {
                    
                    it('Deve criar um registro de retirada de tranca de totem', () => {
                        const registro = new registroRetiradaTrancaTotem({
                            data: new Date(),
                            idTranca: 1,
                            idReparador: 1
                        });
                    
                        expect(registro).toBeInstanceOf(registroRetiradaTrancaTotem);
                        expect(registro.getData()).toEqual({
                            id: registro.getId(),
                            data: registro._data,   
                            idTranca: 1,
                            idReparador: 1
                        });
                    });
            
                    test('Deve retornar um objeto com os dados do registro', () => {
                        const registro = new registroRetiradaTrancaTotem({
                            data: new Date(),
                            idTranca: 1,
                            idReparador: 1
                        });
                    
                        expect(registro.toObject()).toEqual({
                            data: registro._data,   
                            idTranca: 1,
                            idReparador: 1
                        });
                    });
});