const RegistroInsercaoBicicletaTranca = require('../../../src/entities/registroInsercaoBicicletaTranca.entity');
const { idTranca } = require('../../../src/entities/tranca.entity');

describe('RegistroInsercaoBicicletaTranca', () => {
            
            it('Deve criar um registro de inserção de bicicleta em tranca', () => {
                const registro = new RegistroInsercaoBicicletaTranca({
                    data: new Date(),
                    idBicicleta: 1,
                    idTranca: 1
                });
            
                expect(registro).toBeInstanceOf(RegistroInsercaoBicicletaTranca);
                expect(registro.getData()).toEqual({
                    id: registro.getId(),
                    data: registro._data,   
                    idBicicleta: 1,
                    idTranca: 1
                });
            });
    
            test('Deve retornar um objeto com os dados do registro', () => {
                const registro = new RegistroInsercaoBicicletaTranca({
                    data: new Date(),
                    idBicicleta: 1,
                    idTranca: 1
                });
            
                expect(registro.toObject()).toEqual({
                    data: registro._data,   
                    idBicicleta: 1,
                    idTranca: 1
                });
            });
            
});