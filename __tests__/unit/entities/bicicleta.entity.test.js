const BicicletaEntity = require('../../../src/entities/bicicleta.entity.js');


describe('BicicletaEntity', () => {

    it('Deve criar uma entidade de bicicleta', () => {
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "NOVA" ,
        });
    
        expect(bicicleta).toBeInstanceOf(BicicletaEntity);
        expect(bicicleta.getMarca()).toBe('Specialized');
        expect(bicicleta.getModelo()).toBe('Rockhopper');
        expect(bicicleta.getAno()).toBe('2020');
        expect(bicicleta.getStatus()).toBe('NOVA');
    });

    it("Deve integrar a bicicleta na rede", () =>{ 
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "NOVA" ,
        });

        const res = bicicleta.integrarNaRede({}, -1);

        expect(res).toBe(true);
    });

    it("Deve integrar a bicicleta na rede", () =>{ 
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "NOVA"
        });

        const res = bicicleta.integrarNaRede({}, -1);

        expect(res).toBe(true);
    });

    it("Deve retornar erro se a bicicleta está em uso ao tentar integrar ela na rede", () =>{ 
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "EM_USO" ,
            idFuncionario: 1
        });

        const axios = {
            post: jest.fn().mockResolvedValue()
        };

        const res = bicicleta.integrarNaRede(axios, 1);

        expect(res).toBe(false);
        expect(axios.post).toHaveBeenCalled();
    });

    it("Deve retornar erro se a bicicleta está em reparo e o funcionário não é o responsável", () =>{ 
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "EM_REPARO",
            idFuncionario: 1
        });

        const axios = {
            post: jest.fn().mockResolvedValue()
        };

        const res = bicicleta.integrarNaRede(axios, 2);

        expect(res).toBe(false);
        expect(axios.post).not.toHaveBeenCalled();
    });

    test("Deve retornar os dados da bicicleta", () => {
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "EM_REPARO",
            idFuncionario: 1,
            id: expect.any(Number),
            idTranca: expect.any(Number),
            numero: expect.any(Number)
        });

        const res = bicicleta.getData();

        expect(res).toEqual({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "EM_REPARO",
            idFuncionario: 1,
            id: expect.any(Number),
            idTranca: expect.any(Number),
            numero: expect.any(Number)
        });
    });

    test("Deve retornar os dados da bicicleta", () => {
        const bicicleta = new BicicletaEntity({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "EM_REPARO",
            id: expect.any(Number),
            numero: expect.any(Number)
        });

        const res = bicicleta.toObject();

        expect(res).toEqual({
            marca: "Specialized",
            modelo: "Rockhopper",
            ano: "2020",
            status: "EM_REPARO",
            id: expect.any(Number),
            numero: expect.any(Number)
        });
    });

});
