const registroRetiradaBicicletaTranca = require('../../../src/entities/registroRetiradaBicicletaTranca.entity');

describe('RegistroRetiradaBicicletaTranca', () => {
                
                it('Deve criar um registro de retirada de bicicleta de tranca', () => {
                    const registro = new registroRetiradaBicicletaTranca({
                        data: new Date(),
                        idBicicleta: 1,
                        idReparador: 1
                    });
                
                    expect(registro).toBeInstanceOf(registroRetiradaBicicletaTranca);
                    expect(registro.getData()).toEqual({
                        id: registro.getId(),
                        data: registro._data,   
                        idBicicleta: 1,
                        idReparador: 1
                    });
                });
        
                test('Deve retornar um objeto com os dados do registro', () => {
                    const registro = new registroRetiradaBicicletaTranca({
                        data: new Date(),
                        idBicicleta: 1,
                        idReparador: 1
                    });
                
                    expect(registro.toObject()).toEqual({
                        data: registro._data,   
                        idBicicleta: 1,
                        idReparador: 1
                    });
                });
                
    });