require("dotenv").config();
const request = require('supertest');
const { app } = require('../../src/app');
const { idBicicleta } = require("../../src/entities/bicicleta.entity");
const populate = require("../../src/populate.js");

describe('TrancaController', () => {
        beforeAll(() => {
            populate();
        });

        let id = null
    
        describe('/tranca POST', () => {
            it('Deve criar uma tranca', async () => {
                const response = await request(app).post('/tranca').send({
                    localizacao: "Rio de Janeiro",
                    numero: 12345,
                    anoDeFabricacao: 2020,
                    modelo: "Caloi", 
                    status: "NOVA", 
                    idBicicleta: -1,
                    idFuncionario: -1,
                    idTotem: -1,
                    totem: 1
                });
            
                expect(response.status).toBe(200);
                expect(response.body).toHaveProperty('id');
                id = response.body.id;
            });
        });
        
        describe('/trancas GET', () => {
            it('Deve listar as trancas', async () => {
                const response = await request(app).get('/tranca');
            
                expect(response.status).toBe(200);
                expect(response.body).toContainEqual({
                    localizacao: "Rio de Janeiro",
                    numero: 12345,
                    anoDeFabricacao: 2020,
                    modelo: "Caloi", 
                    status: "NOVA", 
                    id
                });
            });
        });
            
        describe('/tranca/:idTranca GET', () => {
            it('Deve retornar uma tranca', async () => {
        
                const response = await request(app).get(`/tranca/${id}`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual({
                    localizacao: "Rio de Janeiro",
                    numero: 12345,
                    anoDeFabricacao: 2020,
                    modelo: "Caloi", 
                    status: "NOVA", 
                    id: 1
                });
            });
        });

        describe('/tranca/:idTranca GET erro', () => {
            it('Deve retornar erro ao retornar uma tranca', async () => {

                const response = await request(app).get(`/tranca/0`);

                expect(response.status).toBe(404);
            });
        });``

        describe('/tranca/integrarNaRede POST', () => {
            it('Deve integrar uma tranca na rede', async () => {
                const response = await request(app).post('/tranca/integrarNaRede')
                .set('Content-Type', 'application/json').send({
                    idTotem: 1,
                    idTranca: id,
                    idFuncionario: 1
                });

                expect(response.status).toBe(200);
            });
        });

        describe('/tranca/integrarNaRede POST erro', () => {
            it('Deve retornar erro ao integrar uma tranca na rede', async () => {
                const response = await request(app).post('/tranca/integrarNaRede')
                .set('Content-Type', 'application/json').send({
                    idTotem: 0,
                    idTranca: id,
                    idFuncionario: 1
                });

                expect(response.status).toBe(422);
            });
        });

        describe('/tranca/:idTranca/trancar POST', () => {
            it('Deve trancar uma tranca', async () => {
                const response = await request(app).post(`/tranca/${id}/trancar`)
                .set('Content-Type', 'application/json').send({
                    idFuncionario: 1
                });
            
                expect(response.status).toBe(200);
            });
        });

        describe('/tranca/:idTranca/trancar POST erro', () => {
            it('Deve retornar erro ao trancar uma tranca', async () => {
                const response = await request(app).post(`/tranca/0/trancar`)
                .set('Content-Type', 'application/json').send({
                    idFuncionario: 1
                });

                expect(response.status).toBe(404);
            });
        });

        describe('/tranca/:idTranca/status/:acao POST', () => {
            it('Deve atualizar o status de uma tranca', async () => {
                const response = await request(app).post(`/tranca/${id}/status/REPARO_SOLICITADO`)
                .set('Content-Type', 'application/json').send({
                    idFuncionario: 1
                });
            
                expect(response.status).toBe(200);
            });
        });

        describe('/tranca/:idTranca/status/:acao POST erro', () => {
            it('Deve retornar erro ao atualizar o status de uma tranca', async () => {
                const response = await request(app).post(`/tranca/0/status/REPARO_SOLICITADO`)
                .set('Content-Type', 'application/json').send({
                    idFuncionario: 1
                });
                
                expect(response.status).toBe(404);
            });
        });

        describe('/tranca/retirarDaRede POST', () => {
            it('Deve retirar uma tranca da rede', async () => {
                const response = await request(app).post('/tranca/retirarDaRede')
                .set('Content-Type', 'application/json').send({
                    idTotem: 1,
                    idTranca: id,
                    idFuncionario: 1
                });

                expect(response.status).toBe(200);
            });
        });

        describe('/tranca/retirarDaRede POST erro', () => {
            it('Deve retornar erro ao retirar uma tranca da rede', async () => {
                const response = await request(app).post('/tranca/retirarDaRede')
                .set('Content-Type', 'application/json').send({
                    idTotem: 0,
                    idTranca: id,
                    idFuncionario: 1
                });

                expect(response.status).toBe(422);
            });
        });

        describe('/tranca/:idTranca/destrancar POST', () => {
            it('Deve destrancar uma tranca', async () => {
                const response = await request(app).post(`/tranca/${id}/destrancar`)
                .set('Content-Type', 'application/json').send({
                    idFuncionario: 1
                });
            
                expect(response.status).toBe(200);
            });
        });

        describe('/tranca/:idTranca/destrancar POST erro', () => {
            it('Deve retornar erro ao destrancar uma tranca', async () => {
                const response = await request(app).post(`/tranca/0/destrancar`)
                .set('Content-Type', 'application/json').send({
                    idFuncionario: 1
                });

                expect(response.status).toBe(404);
            });
        });

        describe('/tranca/:idTranca PUT', () => {
            it('Deve atualizar uma tranca', async () => {
        
                const response = await request(app).put(`/tranca/${id}`).send({
                    localizacao: "Niteroi"
                });
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual({
                    localizacao: "Niteroi",
                    numero: 12345,
                    anoDeFabricacao: 2020,
                    modelo: "Caloi", 
                    status: "DISPONIVEL",
                    id: 1
                });
            });
        });

        describe('/tranca/:idTranca PUT erro', () => {
            it('Deve retornar erro ao atualizar uma tranca', async () => {
        
                const response = await request(app).put(`/tranca/0`).send({
                    localizacao: 'Rua 2'
                });
            
                expect(response.status).toBe(404);
            });
        });

        describe('/tranca/:idTranca/bicicleta GET', () => {
            it('Deve retornar a bicicleta de uma tranca', async () => {
            
                const response = await request(app).get(`/tranca/${id}/bicicleta`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual(expect.arrayContaining([]));
            });
        });

        describe('/tranca/:idTranca/bicicleta GET erro', () => {
            it('Deve retornar erro ao retornar a bicicleta de uma tranca', async () => {
                const response = await request(app).get(`/tranca/0/bicicleta`);
            
                expect(response.status).toBe(404);
            });
        });

        describe('/tranca/:idTranca DELETE', () => {
            it('Deve deletar uma tranca', async () => {
                const response = await request(app).delete(`/tranca/${id}`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual({});
            });
        });

        describe('/tranca/:idTranca DELETE erro', () => {
            it('Deve retornar erro ao deletar uma tranca', async () => {
                const response = await request(app).delete(`/tranca/0`);
            
                expect(response.status).toBe(404);
            });
        });
});
