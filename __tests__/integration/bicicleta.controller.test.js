const request = require('supertest');
const { app } = require('../../src/app');
const populate = require('../../src/populate.js');
require("dotenv").config();

describe('BicicletaController', () => {

    beforeAll(() => {
        populate();
    });

    let id = null;

    describe('/bicicleta POST', () => {
        it('Deve criar uma bicicleta', async () => {
            const response = await request(app).post('/bicicleta').send({
                    marca: "Specialized",
                    modelo: "Rockhopper",
                    ano: "2020",
                    status: "NOVA",
                    numero: 12345   
            });
        
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
            id = response.body.id;
        });
    });
    
    describe('/bicicletas GET', () => {
        it('Deve listar as bicicletas', async () => {
            const response = await request(app).get('/bicicleta');
        
            expect(response.status).toBe(200);
            expect(response.body).toContainEqual({
                marca: "Specialized",
                modelo: "Rockhopper",
                ano: "2020",
                status: "NOVA",
                numero: 12345,
                id
            });
        });
    });
    
    describe('/bicicleta/:idBicicleta GET', () => {
        it('Deve retornar uma bicicleta', async () => {
    
            const response = await request(app).get(`/bicicleta/${id}`);
        
            expect(response.status).toBe(200);
            expect(response.body).toEqual({
                marca: "Specialized",
                modelo: "Rockhopper",
                ano: "2020",
                status: "NOVA",
                numero: 12345,
                id
            });
        });
    });

    describe('/bicicleta/:idBicicleta GET erro', () => {
        it('Deve retornar erro ao retornar uma bicicleta', async () => {

            const response = await request(app).get(`/bicicleta/0`);

            expect(response.status).toBe(404);
        });
    });
    
    describe('/bicicleta/:idBicicleta PUT', () => {
        it('Deve atualizar uma bicicleta', async () => {
    
            const response = await request(app).put(`/bicicleta/${id}`).send({
                modelo: 'Modelo',
                marca: "Marca",
            });
        
            expect(response.status).toBe(200);
            expect(response.body).toEqual({
                modelo: 'Modelo',
                marca: "Marca",
                ano: "2020",
                status: "NOVA",
                numero: 12345,
                id: 1
            });
        });
    });

    describe('/bicicleta/:idBicicleta PUT erro', () => {
        it('Deve retornar erro ao atualizar uma bicicleta', async () => {
    
            const response = await request(app).put(`/bicicleta/0`).send({
                modelo: 'Modelo',
                marca: "Marca",
            });
        
            expect(response.status).toBe(404);
        });
    });

    describe('/bicicleta/integrarNaRede POST', () => {
        it('Deve integrar uma bicicleta na rede', async () => {
            const response = await request(app).post('/bicicleta/integrarNaRede')
            .set('Content-Type', 'application/json').send({
                idBicicleta: id,
                idTranca: 1,
                idFuncionario: 1
            });

            expect(response.status).toBe(200);
        });
    });

    describe('/bicicleta/integrarNaRede POST erro', () => {
        it('Deve retornar erro ao integrar uma bicicleta na rede', async () => {
            const response = await request(app).post('/bicicleta/integrarNaRede')
            .set('Content-Type', 'application/json').send({
                idBicicleta: 0,
                idTranca: 1,
                idFuncionario: 1
            });

            expect(response.status).toBe(422);
        });
    });

    describe('/bicicleta/:idBicicleta/status/:acao POST', () => {
        it('Deve atualizar o status da bicicleta', async () => {
            const response = await request(app).post(`/bicicleta/${id}/status/REPARO_SOLICITADO`)
            .set('Content-Type', 'application/json').send({
                idBicicleta: id
            });

            expect(response.status).toBe(200);
        });
    });

    describe('/bicicleta/:idBicicleta/status/:acao POST erro', () => {
        it('Deve retornar erro ao atualizar o status da bicicleta', async () => {
            const response = await request(app).post(`/bicicleta/0/status/REPARO_SOLICITADO`)
            .set('Content-Type', 'application/json').send({
                idBicicleta: 0
            });

            expect(response.status).toBe(404);
        });
    });

    describe('/bicicleta/retirarDaRede POST', () => {
        it('Deve retirar uma bicicleta da rede', async () => {
            const response = await request(app).post('/bicicleta/retirarDaRede')
            .set('Content-Type', 'application/json').send({
                idBicicleta: id,
                idTranca: 1,
                idFuncionario: 1
            });

            expect(response.status).toBe(200);
        });
    });

    describe('/bicicleta/retirarDaRede POST erro', () => {
        it('Deve retornar erro ao retirar uma bicicleta da rede', async () => {
            const response = await request(app).post('/bicicleta/retirarDaRede')
            .set('Content-Type', 'application/json').send({
                idBicicleta: 0,
                idTranca: 1,
                idFuncionario: 1,
            });

            expect(response.status).toBe(422);
        });
    });

    describe('/bicicleta/:idBicicleta DELETE', () => {
        it('Deve deletar uma bicicleta', async () => {
       
            const response = await request(app).delete(`/bicicleta/${id}`);
        
            expect(response.status).toBe(200);
        });
    });

    describe('/bicicleta/:idBicicleta DELETE erro', () => {
        it('Deve retornar erro ao deletar uma bicicleta', async () => {
       
            const response = await request(app).delete(`/bicicleta/0`);
        
            expect(response.status).toBe(404);
        });
    });

});