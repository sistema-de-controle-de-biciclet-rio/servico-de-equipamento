const request = require('supertest');
const { app } = require('../../src/app');
const populate = require('../../src/populate.js');
require("dotenv").config();

describe('TotemController', () => {
    beforeAll(() => {
        populate();
    });

    let id = null

        describe('/totem POST', () => {
            it('Deve criar um totem', async () => {
                const response = await request(app).post('/totem').send({
                    descricao: "totem x",
                    localizacao: "Rio de Janeiro",
                    idTranca: -1
                });
            
                expect(response.status).toBe(200);
                expect(response.body).toHaveProperty('id');
                id = response.body.id;
            });
        });
    
        describe('/totens GET', () => {
            it('Deve listar os totens', async () => {
                const response = await request(app).get('/totem');
            
                expect(response.status).toBe(200);
                expect(response.body).toContainEqual({
                    descricao: "totem x",
                    localizacao: "Rio de Janeiro",
                    id
                });
            });
        });
        
        describe('/totem/:idTotem GET', () => {
            it('Deve retornar um totem', async () => {
        
                const response = await request(app).get(`/totem/${id}`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual({
                    descricao: "totem x",
                    localizacao: "Rio de Janeiro",
                    id: 1
                });
            });
        });

        describe('/totem/:idTotem GET erro', () => {
            it('Deve retornar erro ao retornar um totem', async () => {

                const response = await request(app).get(`/totem/0`);

                expect(response.status).toBe(404);
            });
        });
        
        describe('/totem/:idTotem PUT', () => {
            it('Deve atualizar um totem', async () => {
        
                const response = await request(app).put(`/totem/${id}`).send({
                    localizacao: 'Rua 2'
                });
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual({
                    descricao: "totem x",
                    localizacao: "Rua 2",
                    id: 1
                });
            });
        });

        describe('/totem/:idTotem PUT erro', () => {
            it('Deve retornar erro ao atualizar um totem', async () => {
        
                const response = await request(app).put(`/totem/0`).send({
                    localizacao: 'Rua 2'
                });
            
                expect(response.status).toBe(404);
            });
        });

        describe('/totem/:idTotem/trancas GET', () => {
            it('Deve listar as trancas de um totem', async () => {
                const response = await request(app).get(`/totem/${id}/trancas`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual(expect.arrayContaining([]));
            });
        });

        describe('/totem/:idTotem/trancas GET erro', () => {
            it('Deve retornar erro ao listar as trancas de um totem', async () => {
                const response = await request(app).get(`/totem/0/trancas`);
            
                expect(response.status).toBe(404);
            });
        });

        describe('/totem/:idTotem/bicicletas GET', () => {
            it('Deve listar as bicicletas de um totem', async () => {

                const response = await request(app).get(`/totem/${id}/bicicletas`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual(expect.arrayContaining([]));
            });
        });

        describe('/totem/:idTotem/bicicletas GET erro', () => {
            it('Deve retornar erro ao listar as bicicletas de um totem', async () => {
                const response = await request(app).get(`/totem/0/bicicletas`);
            
                expect(response.status).toBe(404);
            });
        });
        
        describe('/totem/:idTotem DELETE', () => {
            it('Deve deletar um totem', async () => {
                const totem = await request(app).post('/totem').send({
                    descricao: "totem x",
                    localizacao: "Rio de Janeiro",
                    numero: 12345
                });

                const response = await request(app).delete(`/totem/${totem.body.id}`);
            
                expect(response.status).toBe(200);
                expect(response.body).toEqual({});
            });
        });

        describe('/totem/:idTotem DELETE erro', () => {
            it('Deve retornar erro ao deletar um totem', async () => {
        
                const response = await request(app).delete(`/totem/0`);
            
                expect(response.status).toBe(404);
            });
        });
});