const { readFile, writeFile } = require("fs/promises");



(async () => {
    const _ROOT_ = "./src";

    const routesPath = _ROOT_ + "/routes/";
    const modelPath = _ROOT_ + "/entities/";
    const controllerPath = _ROOT_ + "/controllers/";

    const [executavel, arquivo, nomeRecurso] = process.argv
    
    const nome = nomeRecurso.charAt(0).toUpperCase() + nomeRecurso.slice(1)

    const resourceModel = `
module.exports = class ${nome} {}
    `;

    const resourceController = `
module.exports = class ${nome}Controller {}
    `

    const resourceRoutes = `
const express = require("express");
const router = express.Router();

// crie suas rotas aqui.

module.exports = router;
    `

    const info = {
        model: {
            content: resourceModel,
            path: modelPath
        },
        controller: {
            content: resourceController,
            path: controllerPath
        },
        routes: {
            content: resourceRoutes,
            path: routesPath
        }
    }

    const nomeArquivo = nomeRecurso.charAt(0).toLowerCase() + nomeRecurso.slice(1)

    for (const key in info) {
        try {
            await readFile(info[key].path + `${nomeArquivo}.${key}.js`);

            console.log(`${nomeArquivo}.${key}.js já existe e não será sobrescrito.`);
        } catch (err) {
            if (err.message.match("no such file or directory")) {
                await writeFile(info[key].path + `${nomeArquivo}.${key}.js`, info[key].content)
            }
        }
    }

})();
