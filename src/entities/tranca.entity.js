
module.exports = class Tranca {
    static idTranca = 0;
    static statusTranca = {
        DISPONIVEL: "DISPONIVEL",
        OCUPADA: "OCUPADA",
        EM_REPARO: "EM_REPARO",
        APOSENTADA: "APOSENTADA",
        NOVA: "NOVA",
        REPARO_SOLICITADO: "REPARO_SOLICITADO"
    };

   constructor(dados) {
       
        const { localizacao, numero, anoDeFabricacao, modelo, idFuncionario = -1, idTotem = -1, idBicicleta = -1, status = "NOVA"} = dados;

        /**
        * @private
        * @type {Number}
        */
       this._id = ++Tranca.idTranca;

        /**
        * @private
        * @type {string}
        */
       this._localizacao = localizacao;

        /**
        * @private
        * @type {string}
        */
       this._anoDeFabricacao = anoDeFabricacao;

        /**
        * @private
        * @type {string}
        */
        this._modelo = modelo;


        /**
        * @private
        * @type {Number}
        */
        this._numero = numero;

         /**
        * @private
        * @type
        */
       this._status = status;

       this._idTotem = idTotem;

       this._idFuncionario = idFuncionario;

       this._idBicicleta = idBicicleta;
   }

    getId = () => {
        return this._id;
    }

    getStatus = () => {
        return this._status;
    }   

    getIdFuncionario = () => {
        return this._idFuncionario;
    }

    getIdTotem = () => {
        return this._idTotem;
    }

    getIdBicicleta = () => {
        return this._idBicicleta;
    }

    getLocalizacao = (localizacao) => {
        return this._localizacao;
    }

    getAnoDeFabricacao = () => {
        return this._anoDeFabricacao;
    }

    getModelo = () => {
        return this._modelo;
    }

    getNumero = () => {
        return this._numero;
    }

    getData = () => {
        return {
            id: this._id,
            localizacao: this._localizacao,
            anoDeFabricacao: this._anoDeFabricacao,
            modelo: this._modelo,
            status: this._status,
            idTotem: this._idTotem,
            idFuncionario: this._idFuncionario,
            idBicicleta: this._idBicicleta,
            numero: this._numero
       }
   }

   toObject = () => {
    return {
        id: this._id,
        localizacao: this._localizacao,
        anoDeFabricacao: this._anoDeFabricacao,
        modelo: this._modelo,
        status: this._status,
        numero: this._numero,
    }
}

    integrarNaRede = (idFuncionario) => {
        if(!this.nova() && !this.emReparo()) {
            return false;
        }

        return !this.emReparo() || Number.parseInt(idFuncionario) === this._idFuncionario
    }

    retirarDaRede = () => {
        return this.aguardandoReparo() && this._idBicicleta === -1 && this._idTotem !== -1;
    }

    nova = () => {
        return this._status === Tranca.statusTranca.NOVA;
    }

    emReparo = () => {
        return this._status === Tranca.statusTranca.EM_REPARO;
    }

    aguardandoReparo = () => {
        return this._status === Tranca.statusTranca.REPARO_SOLICITADO;
    }
}


    