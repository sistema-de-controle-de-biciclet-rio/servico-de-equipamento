
module.exports = class RegistroInsercaoBicicletaTranca {
    static idRegistroInsercaoBicicletaTranca = 0;

    constructor(dados) {
        
        const { data, idTranca, idBicicleta } = dados;

         /**
         * @private
         * @type {Number}
         */
        this._id = ++RegistroInsercaoBicicletaTranca.idRegistroInsercaoBicicletaTranca;

         /**
         * @private
         * @type {Date}
         */
        this._data = data;

          /**
         * @private
         * @type {Number}
         */
          this._idTranca = idTranca;

            /**
         * @private
         * @type {Number}
         */
            this._idBicileta = idBicicleta;
    }

    getId = () => {
        return this._id;
    }

    getData = () => {
        return {
            id: this._id,
            data: this._data,   
            idTranca: this._idTranca,
            idBicicleta: this._idBicileta
        }
    }

    toObject = () => {
        return {
            data: this._data,   
            idTranca: this._idTranca,
            idBicicleta: this._idBicileta
        }
    }
}