
module.exports = class Totem {
    static idTotem = 0;

     /**
     * @param {{localizacao: string, descricao: string}} dados 
     */
    constructor(dados) {
        
        const { localizacao, descricao} = dados;

         /**
         * @private
         * @type {Number}
         */
        this._id = ++Totem.idTotem;

         /**
         * @private
         * @type {string}
         */
        this._localizacao = localizacao;

         /**
         * @private
         * @type {string}
         */
        this._descricao = descricao;

    }

    getId = () => {
        return this._id;
    }

    getData = () => {
        return {
            id: this._id,
            localizacao: this._localizacao,
            descricao: this._descricao,
        }
    }
}