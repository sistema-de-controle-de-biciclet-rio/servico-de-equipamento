
module.exports = class RegistroRetiradaTrancaTotem {
    static idRegistroRetiradaTrancaTotem = 0;

    constructor(dados) {
        
        const { data, idTranca, idReparador } = dados;

         /**
         * @private
         * @type {Number}
         */
        this._id = ++RegistroRetiradaTrancaTotem.idRegistroRetiradaTrancaTotem;

         /**
         * @private
         * @type {Date}
         */
        this._data = data;

          /**
         * @private
         * @type {Number}
         */
          this._idTranca = idTranca;

           /**
         * @private
         * @type {Number}
         */
           this._idReparador = idReparador;
    }

    getId = () => {
        return this._id;
    }

    getData = () => {
        return {
            id: this._id,
            data: this._data,   
            idTranca: this._idTranca,
            idReparador: this._idReparador
        }
    }

    toObject = () => {
        return {
            data: this._data,   
            idTranca: this._idTranca,
            idReparador: this._idReparador
        }
    }
}