
module.exports = class RegistroRetiradaBicicletaTranca {
    static idRegistroRetiradaBicicletaTranca = 0;

    constructor(dados) {
        
        const { data, idReparador, idBicicleta} = dados;

         /**
         * @private
         * @type {Number}
         */
        this._id = ++RegistroRetiradaBicicletaTranca.idRegistroRetiradaBicicletaTranca;

         /**
         * @private
         * @type {Date}
         */
        this._data = data;

          /**
         * @private
         * @type {Number}
         */
          this._idReparador = idReparador;

            /**
         * @private
         * @type {Number}
         */
            this._idBicileta = idBicicleta;
    }

    getId = () => {
        return this._id;
    }

    getData = () => {
        return {
            id: this._id,
            data: this._data,   
            idReparador: this._idReparador,
            idBicicleta: this._idBicileta
        }
    }

    toObject = () => {
        return {
            data: this._data,   
            idReparador: this._idReparador,
            idBicicleta: this._idBicileta
        }
    }
}