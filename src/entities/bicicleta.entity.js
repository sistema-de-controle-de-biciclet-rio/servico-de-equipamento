require("dotenv").config();

module.exports = class Bicicleta {
    static idBicicleta = 0;
    static statusBicicleta = {
        EM_USO: "EM_USO",
        DISPONIVEL: "DISPONIVEL",
        NOVA: "NOVA",
        APOSENTADA: "APOSENTADA",
        REPARO_SOLICITADO: "REPARO_SOLICITADO",
        EM_REPARO: "EM_REPARO"
    };

    constructor(dados) {

        const { marca, numero, modelo, ano, idTranca = -1, idFuncionario = -1, status = "NOVA" } = dados;

        /**
         * @private
         * @type {Number}
         */
        this._id = ++Bicicleta.idBicicleta;

        /**
         * @private
         * @type {string}
         */
        this._marca = marca;

        /**
         * @private
         * @type {string}
         */
        this._modelo = modelo;

        /**
         * @private
         * @type {string}
         */
        this._ano = ano;

        /**
         * @private
         * @type {Number}
         */
        this._numero = numero;

        /**
         * @private
         * @type
         */

         this._idTranca = idTranca;

         this._idFuncionario = idFuncionario;

         this._status = status;
    }

    getId = () => {
        return this._id;
    }

    getIdFuncionario = () => {
        return this._idFuncionario;
    }

    getStatus = () => {
        return this._status;
    }

    getIdTranca = () => { 
        return this._idTranca;
    }

    getMarca = () => {
        return this._marca;
    }

    getAno = () => {
        return this._ano;
    }

    getModelo = () => {
        return this._modelo;
    }

    getNumero = () => {
        return this._numero;
    }

    getData = () => {
        return {
            id: this._id,
            marca: this._marca,
            modelo: this._modelo,
            ano: this._ano,
            status: this._status,
            idTranca: this._idTranca,
            idFuncionario: this._idFuncionario,
            numero: this._numero
        }
    }

    toObject = () => {
        return {
            id: this._id,
            marca: this._marca,
            modelo: this._modelo,
            ano: this._ano,
            status: this._status,
            numero: this._numero,
        }
    }

    integrarNaRede = (axios, idFuncionario) => {
        if(this.emUso()) {
            axios.post(`${process.env.ALUGUEL_URL}/devolucao`, {idTranca: this._idTranca, idBicicleta: this._id});
            return false;
        }
        
        return (!this.emReparo() || idFuncionario === this._idFuncionario); 
    }

    emUso = () => { 
        return this._status ===  Bicicleta.statusBicicleta.EM_USO
    }

    emReparo = () => {
        return this._status === Bicicleta.statusBicicleta.EM_REPARO
    }

}
