
const express = require("express");
const router = express.Router();
const BicicletaController = require("../controllers/bicicleta.controller.js");
const DataSource = require("../datasource.js");
const AxiosService = require("../axios.service.js");

const RegistroInsercaoBicicletaTrancaRepository = DataSource.getRepository("registroInsercaoBicicletaTranca");
const registroRetiradaBicicletaTrancaRepository = DataSource.getRepository("registroRetiradaBicicletaTranca");
const axiosService = new AxiosService({ options: { headers: { "Content-Type": "application/json" } }});
const bicicletaRepository = DataSource.getRepository("bicicleta");
const trancaRepository = DataSource.getRepository("tranca");
const bicicletaController = new BicicletaController(bicicletaRepository, axiosService, RegistroInsercaoBicicletaTrancaRepository, trancaRepository, registroRetiradaBicicletaTrancaRepository);

router.post("/bicicleta", bicicletaController.postBicicleta);
router.post("/bicicleta/integrarNaRede", bicicletaController.integrarBicicletaNaRede);
router.post("/bicicleta/retirarDaRede", bicicletaController.retirarBicicletaDaRede);
router.post("/bicicleta/:idBicicleta/status/:acao", bicicletaController.atualizarStatusBicicleta);
router.get("/bicicleta", bicicletaController.getBicicletas);
router.get("/bicicleta/:idBicicleta", bicicletaController.getBicicleta);
router.put("/bicicleta/:idBicicleta", bicicletaController.atualizarBicicleta);
router.delete("/bicicleta/:idBicicleta", bicicletaController.deleteBicicleta);


module.exports = router;
    
