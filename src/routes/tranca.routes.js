const express = require("express");
const router = express.Router();
const TrancaController = require("../controllers/tranca.controller.js");
const DataSource = require("../datasource.js");
const AxiosService = require("../axios.service.js");

const axiosService = new AxiosService();
const trancaRepository = DataSource.getRepository("tranca");
const RegistroInsercaoTrancaTotemRepository = DataSource.getRepository("registroInsercaoTrancaTotem");
const registroRetiradaTrancaTotemRepository = DataSource.getRepository("registroRetiradaTrancaTotem");
const totemRepository = DataSource.getRepository("totem");
const bicicletaRepository = DataSource.getRepository("bicicleta");
const trancaController = new TrancaController(trancaRepository, axiosService, RegistroInsercaoTrancaTotemRepository, totemRepository, registroRetiradaTrancaTotemRepository, bicicletaRepository);

router.post("/tranca", trancaController.postTranca);
router.post("/tranca/integrarNaRede", trancaController.integrarTrancaNaRede);
router.post("/tranca/retirarDaRede", trancaController.retirarTrancaDaRede);
router.post("/tranca/:idTranca/trancar", trancaController.trancar);
router.post("/tranca/:idTranca/destrancar", trancaController.destrancar);
router.post("/tranca/:idTranca/status/:acao", trancaController.atualizarStatusTranca);
router.get("/tranca", trancaController.getTrancas);
router.get("/tranca/:idTranca", trancaController.getTranca);
router.get("/tranca/:idTranca/bicicleta", trancaController.getBicicletaTranca);
router.put("/tranca/:idTranca", trancaController.atualizarTranca);
router.delete("/tranca/:idTranca", trancaController.deleteTranca);


module.exports = router;
    