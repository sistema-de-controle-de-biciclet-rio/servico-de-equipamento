
const express = require("express");
const router = express.Router();
const TotemController = require("../controllers/totem.controller.js");
const DataSource = require("../datasource.js");

const totemRepository = DataSource.getRepository("totem");
const trancaRepository = DataSource.getRepository("tranca");
const bicicletaRepository = DataSource.getRepository("bicicleta");

const totemController = new TotemController(totemRepository, trancaRepository, bicicletaRepository);

router.post("/totem", totemController.postTotem);
router.get("/totem", totemController.getTotens);
router.get("/totem/:idTotem", totemController.getTotem);
router.get("/totem/:idTotem/trancas", totemController.getTrancasTotem);
router.get("/totem/:idTotem/bicicletas", totemController.getBicicletasTotem);
router.put("/totem/:idTotem", totemController.atualizarTotem);
router.delete("/totem/:idTotem", totemController.deleteTotem);


module.exports = router;
    