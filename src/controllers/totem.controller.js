const { idBicicleta } = require("../entities/bicicleta.entity");

module.exports = class TotemController {

     /**
     * @private
     * 
     * @param {Repository} repository 
     */
    constructor(repository, trancaRepository, bicicletaRepository) {
        /**
             * @type {Repository} totemRepository
             */
        this.totemRepository = repository;
        this.trancaRepository = trancaRepository;
        this.bicicletaRepository = bicicletaRepository;
    }

 /**
     * @param {import("express").Request} request 
     * @param {import("express").Response} response 
     */
    postTotem = async (request, response) => {
        const body = request.body;
        
        try {
            const totem = await this.totemRepository.salvar(body);
            response.status(200).json(totem.getData());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    getTotens = (request, response) => {
        const totens = this.totemRepository.buscarTodas();
        response.status(200).json(totens.map(totem => totem.getData()));
    }

    getTotem = (request, response) => {
        const id = request.params.idTotem;
        const totem = this.totemRepository.buscarPorId(id);

        if (totem === null) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Totem não encontrado."
            });
        }
        
        return response.status(200).json(totem.getData());
    }

    getTrancasTotem = (request, response) => {
        const id = request.params.idTotem;
        const totem = this.totemRepository.buscarPorId(id);

        if (totem === null) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Totem não encontrado."
            });
        }

        const trancas = this.trancaRepository.buscarTodas({ idTotem: Number.parseInt(id) });

        if (trancas.length === 0) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Nenhuma tranca encontrada."
            });
        }
        
        return response.status(200).json(trancas.map(tranca => tranca.toObject()));
    }

    getBicicletasTotem = (request, response) => {
        const id = request.params.idTotem;
        const totem = this.totemRepository.buscarPorId(id);

        if (totem === null) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Totem não encontrado."
            });
        }
        let bicicletas = [];

        const trancas = this.trancaRepository.buscarTodas({ idTotem: Number.parseInt(id) });

        trancas.forEach(tranca => { 
            const bicicleta = this.bicicletaRepository.buscarTodas({ idTranca: Number.parseInt(tranca._id) })
            if (bicicleta.length > 0) {
            bicicletas.push(
                bicicleta[0]
            );
            }
        });

        if (bicicletas.length === 0) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Nenhuma bicicleta encontrada."
            });
        }
        
        return response.status(200).json(bicicletas.map(bicicleta => bicicleta.toObject()));
    }

    deleteTotem = (request, response) => {
        const id = request.params.idTotem;

        const trancas = this.trancaRepository.buscarTodas({ idTotem: Number.parseInt(id) });

        if (trancas.length !== 0) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Totem nao pode ser removido, pois possui trancas associadas`."
            });
        }

        const resultado = this.totemRepository.remover(id);

        if (resultado === false) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Totem não encontrado."
            });
        }
        return response.status(200).end();
    }

    atualizarTotem = async (request, response) => {
        const id = request.params.idTotem;
        const body = request.body;
    
        try {
            const totem = this.totemRepository.atualizar(id, body);
            if (totem === null) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Totem não encontrado."
                });
            }
            response.status(200).json(totem.getData());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }
}