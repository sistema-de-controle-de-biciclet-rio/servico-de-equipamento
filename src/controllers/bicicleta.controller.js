require("dotenv").config();

module.exports = class BicicletaController {

    /**
     * @private
     * 
     * @param {Repository} repository 
     */
    constructor(repository, axiosService, registroInsercaoBicicletaTrancaRepository, trancaRepository, registroRetiradaBicicletaTrancaRepository) {

        /**
         * @type {repository} bicicletaRepository
         */
        this.axios = axiosService;
        this.bicicletaRepository = repository;
        this.registroInsercaoBicicletaTrancaRepository = registroInsercaoBicicletaTrancaRepository;
        this.trancaRepository = trancaRepository;
        this.registroRetiradaBicicletaTrancaRepository = registroRetiradaBicicletaTrancaRepository;
    }

    /**
     * @param {import("express").Request} request 
     * @param {import("express").Response} response 
     */
    postBicicleta = async (request, response) => {
        const body = request.body;
        
        try {
            const bicicleta = await this.bicicletaRepository.salvar(body);
            response.status(200).json(bicicleta.toObject());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }

    }

    getBicicletas = (request, response) => {
        const bicicletas = this.bicicletaRepository.buscarTodas();
        response.status(200).json(bicicletas.map(bicicleta => bicicleta.toObject()));
    }

    getBicicleta = (request, response) => {
        const id = request.params.idBicicleta;
        const bicicleta = this.bicicletaRepository.buscarPorId(id);

        if (bicicleta === null) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Bicicleta não encontrada."
            });
        }
        
        return response.status(200).json(bicicleta.toObject());
    }

    deleteBicicleta = (request, response) => {
        const id = request.params.idBicicleta;
        const bicicleta = this.bicicletaRepository.buscarPorId(id);

        if (bicicleta === null) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Bicicleta não encontrada."
            });
        }

        if(bicicleta._status !==  "APOSENTADA" && bicicleta._idTranca !== -1) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Bicicleta não pode ser excluida, pois está em uma tranca ou não está aposentada."
            });
        }

        this.bicicletaRepository.remover(id);

        return response.status(200).end();
    }

    atualizarBicicleta = async (request, response) => {
        const id = request.params.idBicicleta;
        const body = request.body;

        if (body.hasOwnProperty('status')) {
            delete body.status;
        }
    
        try {
            const bicicleta = this.bicicletaRepository.atualizar(id, body);
            if (bicicleta === null) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Bicicleta não encontrada."
                });
            }
            response.status(200).json(bicicleta.toObject());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    atualizarStatusBicicleta = async (request, response) => {
        const id = request.params.idBicicleta;
        const status = request.params.acao;
    
        try {
            const bicicleta = this.bicicletaRepository.atualizar(id, {status});
            if (bicicleta === null) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Bicicleta não encontrada."
                });
            }
            response.status(200).json(bicicleta.toObject());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    integrarBicicletaNaRede = async (request, response) => {
        const body = request.body;
        const id = body.idBicicleta;

        const bicicleta = this.bicicletaRepository.buscarPorId(id);

        if (bicicleta === null) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Bicicleta não encontrada."
            }]);
        }

        if (!bicicleta.integrarNaRede(this.axios, body.idFuncionario)) {
            return response.status(422).json([{
            codigo: 422,
            mensagem: "A bicicleta ou está em uso, ou está em reparo e o funcionário que tentou integra-la nao foi o mesmo que a retirou da rede."
            }]);
        };

        const conteudo = await this.registroInsercaoBicicletaTrancaRepository.salvar({data: (new Date ()).toLocaleString(), idTranca: body.idTranca, idBicicleta: id});
        
        this.bicicletaRepository.atualizar(id, {idTranca: body.idTranca, status: "DISPONIVEL"});
        this.trancaRepository.atualizar(body.idTranca, {idBicicleta: id, status: "OCUPADA"});
        
        await this.#notificaFuncionario(body, response, conteudo, "Bicicleta integrada na rede");

        response.status(200).end();
    }

    retirarBicicletaDaRede = async (request, response) => {
        const body = request.body;
        const id = body.idBicicleta;
        const tranca = this.trancaRepository.buscarPorId(body.idTranca);

        const bicicleta = this.bicicletaRepository.buscarPorId(id);

        if (bicicleta === null) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Bicicleta não encontrada."
            }]);
        }

        if (tranca === null) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Tranca não encontrada."
            }]);
        }

        if (bicicleta.getIdTranca() === -1) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Bicicleta não está na rede."
            }]);
        }

        if(bicicleta.getStatus() !== "REPARO_SOLICITADO") {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Não é possível retirar bicicleta da rede, pois é necessário solicitar o reparo antes."
            }]);
        }    

        let status = "";

        if(body.status === "APOSENTADA") {
            status = "APOSENTADA";
        }

        else if(body.status === "EM_REPARO") {
            status = "EM_REPARO";
        }
        
        const conteudo = await this.registroRetiradaBicicletaTrancaRepository.salvar({data: (new Date ()).toLocaleString(), idReparador: body.idFuncionario, idBicicleta: id});
        
        this.trancaRepository.atualizar(tranca.getId(), {idBicicleta: -1, status: "DISPONIVEL"});

        this.bicicletaRepository.atualizar(id, {idTranca: -1, status});

        await this.#notificaFuncionario(body, response, conteudo, "Bicicleta retirada da rede");

        response.status(200).end();
    }

    #notificaFuncionario = async (body, response, conteudo, assunto) => {
        const resFuncionario = await this.axios.get(`${process.env.ALUGUEL_URL}/funcionario/${body.idFuncionario}`);
        if (!resFuncionario.success) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Funcionário não encontrado."
            }]);
        }
        
        const resEmail = await this.axios.post(`${process.env.EXTERNO_URL}/enviarEmail`, {
            assunto: assunto,
            mensagem: JSON.stringify(conteudo.toObject()),
            email: resFuncionario.data.email
        });

        if (!resEmail.success) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Erro ao enviar email."
            }]);
        }
    }

}