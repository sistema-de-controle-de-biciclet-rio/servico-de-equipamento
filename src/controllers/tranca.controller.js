require("dotenv").config();

module.exports = class TrancaController {

    /**
     * @private
     * 
     * @param {Repository} repository 
     */
    constructor(repository, axiosService, registroInsercaoTrancaTotemRepository, totemRepository, registroRetiradaTrancaTotemRepository, bicicletaRepository) {
        /**
             * @type {Repository} trancaRepository
             */
        this.trancaRepository = repository;
        this.axios = axiosService;
        this.registroInsercaoTrancaTotemRepository = registroInsercaoTrancaTotemRepository;
        this.totemRepository = totemRepository;
        this.registroRetiradaTrancaTotemRepository = registroRetiradaTrancaTotemRepository;
        this.bicicletaRepository = bicicletaRepository;
    }
    
    /**
     * @param {import("express").Request} request 
     * @param {import("express").Response} response 
     */

    postTranca = async (request, response) => {
        const body = request.body;
        
        try {
            const tranca = await this.trancaRepository.salvar(body);
            response.status(200).json(tranca.toObject());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    getTrancas = (request, response) => {
        const trancas = this.trancaRepository.buscarTodas();
        response.status(200).json(trancas.map(tranca => tranca.toObject()));
    } 

    getTranca = (request, response) => {
        const id = request.params.idTranca;
        const tranca = this.trancaRepository.buscarPorId(id);

        if (tranca === null) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Tranca não encontrada."
            });
        }
        
        return response.status(200).json(tranca.toObject());
    }

    deleteTranca = (request, response) => {
        const id = request.params.idTranca;
        const tranca = this.trancaRepository.buscarPorId(id);

        const resultado = this.trancaRepository.remover(id);

        if (resultado === false) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Tranca não encontrada."
            });
        }

        if(tranca._idBicicleta !== -1) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Tranca não pode ser excluida pois está ocupada."
            });
        }

        return response.status(200).end();
    }

    atualizarTranca = async (request, response) => {
        const id = request.params.idTranca;
        const body = request.body;

        if (body.hasOwnProperty('status')) {
            delete body.status;
        }
    
        try {
            const tranca = this.trancaRepository.atualizar(id, body);
            if (tranca === null) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Tranca não encontrada."
                });
            }
            response.status(200).json(tranca.toObject());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    integrarTrancaNaRede = async (request, response) => {
        const body = request.body;
        const id = body.idTranca;
        const totem = this.totemRepository.buscarPorId(body.idTotem);
        const tranca = this.trancaRepository.buscarPorId(id);

        if (!this.validateTrancaAndTotem(tranca, totem, response)) {
            return;
        }
        
        if (!tranca.integrarNaRede(body.idFuncionario)) {
            return response.status(422).json([{
            codigo: 422,
            mensagem: "Funcionário não autorizado."
            }]);
        };

        const res = await this.axios.get(`${process.env.ALUGUEL_URL}/funcionario/${body.idFuncionario}`);
        
        const conteudo = await this.registroInsercaoTrancaTotemRepository.salvar({data: (new Date ()).toLocaleString(), idTranca: id, idReparador: body.idFuncionario});
        
        this.trancaRepository.atualizar(id, {idTotem: body.idTotem, status: "DISPONIVEL"});

        if(!res.success) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Funcionário não encontrado."
            }]);
        }

        await this.#notificaFuncionario(res.data.email, response, conteudo, "Tranca integrada na rede");
   
        response.status(200).end();
    }

    retirarTrancaDaRede = async (request, response) => {
        const body = request.body;
        const id = body.idTranca;
        const totem = this.totemRepository.buscarPorId(body.idTotem);

        const tranca = this.trancaRepository.buscarPorId(id);

        if (!this.validateTrancaAndTotem(tranca, totem, response)) {
            return;
        }

        if(!tranca.retirarDaRede()) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Para retirar a tranca da rede, é necessário que ela esteja com o status de reparo solicitado, esteja associada a um totem e nao esteja associada a nenhuma bibicleta."
            }]);
        }

        let status = "";

        if(body.statusAcaoReparador === "EM_REPARO") {
            status = "EM_REPARO";
        }

        else status = "APOSENTADA";

        const res = await this.axios.get(`${process.env.ALUGUEL_URL}/funcionario/${body.idFuncionario}`);

        const conteudo = await this.registroRetiradaTrancaTotemRepository.salvar({data: (new Date ()).toLocaleString(), idReparador: body.idFuncionario, idTranca: id});

        this.trancaRepository.atualizar(id, {idTotem: -1, status, idFuncionario: body.idFuncionario});

        if(!res.success) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Funcionário não encontrado."
            }]);
        }

        await this.#notificaFuncionario(res.data.email, response, conteudo, "Tranca retirada da rede");
 
        response.status(200).end();
    }
   
    destrancar = async (request, response) => {
        const id = request.params.idTranca;
      
    
        try {
            const tranca = this.trancaRepository.atualizar(id, {status: "DISPONIVEL"});
            if (tranca === null) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Tranca não encontrada."
                });
            }
            
            await this.registroRetiradaTrancaTotemRepository.salvar({data: (new Date ()).toLocaleString(), idReparador: this._idFuncionario, idTranca: id});

            response.status(200).json(tranca.toObject());

        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }

    trancar = async (request, response) => {
        const id = request.params.idTranca;
    
        try {
            const tranca = await this.trancaRepository.atualizar(id, { status: "OCUPADA" });
    
            if (!tranca) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Tranca não encontrada."
                });
            }

            await this.registroInsercaoTrancaTotemRepository.salvar({data: (new Date ()).toLocaleString(), idTranca: id, idReparador: this._idFuncionario});

            response.status(200).json(tranca.toObject());

        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }
    }
    
    atualizarStatusTranca = async (request, response) => {
        const id = request.params.idTranca;
        const status = request.params.acao;
    
        try {
            const tranca = this.trancaRepository.atualizar(id, {status});
            if (tranca === null) {
                return response.status(404).json({
                    codigo: 404,
                    mensagem: "Tranca não encontrada."
                });
            }
            response.status(200).json(tranca.toObject());
        } catch (err) {
            response.status(422).json([{
                codigo: 422,
                mensagem: err.message
            }]);
        }

    }

    getBicicletaTranca = (request, response) => {
        const id = request.params.idTranca;
        const tranca = this.trancaRepository.buscarPorId(id);

        if (tranca === null) {
            return response.status(404).json({
                codigo: 422,
                mensagem: "Id da tranca inválido."
            });
        }

        const bicicletas = this.bicicletaRepository.buscarTodas({ idTranca: Number.parseInt(id) });

        if(bicicletas.length === 0) {
            return response.status(404).json({
                codigo: 404,
                mensagem: "Nenhuma bicicleta encontrada."
            });
        }
        
        return response.status(200).json(bicicletas.map(bicicleta => bicicleta.toObject()));
    }

    validateTrancaAndTotem(tranca, totem, response) {
        if (tranca === null) {
            response.status(422).json([{
                codigo: 422,
                mensagem: "Tranca não encontrada."
            }]);
            return false;
        }

        if (totem === null) {
            response.status(422).json([{
                codigo: 422,
                mensagem: "Totem não encontrado."
            }]);
            return false;
        }

        return true;
    }

    #notificaFuncionario = async (email, response, conteudo, assunto) => {
        
        const resEmail = await this.axios.post(`${process.env.EXTERNO_URL}/enviarEmail`, {
            assunto: assunto,
            mensagem: JSON.stringify(conteudo.toObject()),
            email: email
        });

        if (!resEmail.success) {
            return response.status(422).json([{
                codigo: 422,
                mensagem: "Erro ao enviar email."
            }]);
        }
    }
}
    