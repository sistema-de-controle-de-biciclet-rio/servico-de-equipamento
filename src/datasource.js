const { readdir } = require("fs/promises");

const DataSource = {
    tabelas: {},
    initialize:  async (source = []) => {
        let files = [];

        if (source.length === 0) {
            files = await readdir("./src/entities");
        } else {
            files = source;
        }

        for (const file of files) {
            const entity = file.split(".")[0];

            DataSource.tabelas[entity] = []
        }
    },    
    clear: async () => {
        for (const tabela in DataSource.tabelas) {
            DataSource.tabelas[tabela] = [];

            const module = await import(`./entities/${tabela}.entity.js`);
            const { default: myDefault } = module;
            myDefault["id" + tabela.charAt(0).toUpperCase() + tabela.slice(1)] = 0;
        }
    },
    getRepository: (tabela) => {
        return {
            salvar: async (dados) => {
                const module = await import(`./entities/${tabela}.entity.js`);

                const { default: myDefault } = module;
        
                const entity = new myDefault(dados);
        
                DataSource.tabelas[tabela].push(entity);
        
                return entity;
            },
            buscarTodas: (criterios = {}) => {
                const entidades = DataSource.tabelas[tabela];

                if (Object.keys(criterios).length === 0) {
                    return entidades;
                }

                const filtro = entidades.filter(entidade => {
                    for (const criterio in criterios) {
                        if (!(`_${criterio}` in entidade)) {
                            throw Error(`Propriedade ${criterio} não existe em ${tabela}.`);
                        }
                       
                        if (entidade[`_${criterio}`] === criterios[criterio]) {
                            return true;
                        }
                    }

                    return false;
                });

                return filtro;
            },
            buscarPorId: (id, copy = true) => {
                const idNumerico = Number.parseInt(id);

                const entidade = DataSource.tabelas[tabela].filter(elemento => {
                    return elemento.getId() === idNumerico;
                });

                if (entidade.length === 0) return null;

                return copy ? { ...entidade[0]} : entidade[0];
            },
            remover(id) {
                const entidade = this.buscarPorId(id, false);

                if (entidade === null) {
                    return false;
                }

                const posicao = DataSource.tabelas[tabela].indexOf(entidade);
                
                DataSource.tabelas[tabela].splice(posicao, 1);

                return true;
            },
            persistir(id, entidade) {
                const original = this.buscarPorId(id, false);

                for (const key in original) {
                    original[key] = entidade[key];
                }
            },  
            atualizar(id, payload) {
                const entidade = this.buscarPorId(id, false);

                if (entidade === null) return null;

                const dados = entidade.getData();

                for (const key in payload) {
                    let propriedadeExiste = false;

                    for (const prop in dados) {
                        if (prop === key) {
                            propriedadeExiste = true;
                        }
                    }

                    if (!propriedadeExiste) {
                        throw Error("Propriedade inválida: ${key}");
                    }

                    entidade["_" + key] = payload[key];
                }

                return entidade;
            }   
        }
    }
}

DataSource.initialize();

module.exports = DataSource;
