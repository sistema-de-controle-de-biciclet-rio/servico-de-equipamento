const express = require("express");
const bicicletaRoutes = require("./routes/bicicleta.routes.js");
const totemRoutes = require("./routes/totem.routes.js");
const trancaRoutes = require("./routes/tranca.routes.js");
const DataSource = require("./datasource.js");
const app = express();
const populate = require("./populate.js");

app.use(express.json());
app.disable("x-powered-by");

app.use(bicicletaRoutes);
app.use(totemRoutes);
app.use(trancaRoutes);
app.get("/restaurarDados", async (req, res) => {
    try {
        await DataSource.clear();
        await populate();
    } catch (err) {
        return res.status(500).json({ mensagem: err.message });
    }

    return res.status(200).end();
});

module.exports = { app };