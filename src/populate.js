const Datasource = require("./datasource.js");
const fs = require("node:fs/promises");


module.exports = async () => {
    const [
        bibicletaRepository, 
        trancaRepository, 
        totemRepository,
        registroRetiradaBicicletaTrancaRepository,
        registroRetiradaTrancaTotemRepository
    ] = [
        "bicicleta", 
        "tranca", 
        "totem", 
        "registroRetiradaBicicletaTranca",
        "registroRetiradaTrancaTotem"
    ].map(nome => {
        return Datasource.getRepository(nome);
    });

    const { 
        trancas, 
        totens, 
        bicicletas, 
        registrosRetiradaBicicletaTotem,
        registrosRetiradaTrancaTotem  
    } = JSON.parse((await fs.readFile("./amostra-equipamento.json", { encoding: "utf-8" })));

    trancas.forEach(tranca => {
        trancaRepository.salvar(tranca);
    });

    totens.forEach(totem => {
        totemRepository.salvar(totem);
    });

    bicicletas.forEach(bicicleta => {
        bibicletaRepository.salvar(bicicleta);
    });

    registrosRetiradaBicicletaTotem.forEach(registro => {
        registroRetiradaBicicletaTrancaRepository.salvar({ ...registro, data: new Date() });
    });

    registrosRetiradaTrancaTotem.forEach(registro => {
        registroRetiradaTrancaTotemRepository.salvar({ ...registro, data: new Date() });
    });
};