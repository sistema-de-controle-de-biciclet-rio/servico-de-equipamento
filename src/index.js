require("dotenv").config();
const { app } = require("./app.js");
const populate = require("./populate.js");

populate().then(() => {
    app.listen(process.env.APP_PORT, () => console.log("Listening on port " + process.env.APP_PORT));
});